<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>【Item Update】</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"></li>
		</ul>
		<ul class="navbar-nav">
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link text-danger" href="LogoutServlet">ログアウト</a></li>
		</ul>
	</nav>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-auto mx-auto">
				<h2>
					<strong>Item Update</strong>
				</h2>
			</div>
		</div>
		<br>
		<form action="ItemUpdateServlet" method="post">

			<c:forEach var="item" items="${item}">
				<input type="hidden" value="${item.id}" name="id">
				<div class="row">
					<div class="col-auto mx-auto">
						<table class="table-bordered">

							<tr class="table-secondary">
								<th style="width: 1200px;" class="text-center">アイテム名</th>
							</tr>

							<tr>
								<th class="text-center"><input type="text" name="name"
									class="form-control" value="${item.name }" required></th>
							</tr>

						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-auto mx-auto">
						<table class="table-bordered">

							<tr class="table-secondary">
								<th style="width: 1200px;" class="text-center">カテゴリー</th>
							</tr>

							<tr>
								<th class="text-center"><select class="form-control"
									name="category" id="sel1">
										<option ${c1}>【材料/換金アイテム】</option>
										<option ${c2}>【使用できるアイテム】</option>
										<option ${c3}>【片手剣】</option>
										<option ${c4}>【両手剣】</option>
										<option ${c5}>【抜刀剣】</option>
										<option ${c6}>【旋風槍】</option>
										<option ${c7}>【杖】</option>
										<option ${c8}>【魔導具】</option>
										<option ${c9}>【手甲】</option>
										<option ${c10}>【弓】</option>
										<option ${c11}>【自動弓】</option>
										<option ${c12}>【矢】</option>
										<option ${c13}>【短剣】</option>
										<option ${c14}>【盾】</option>
										<option ${c15}>【体防具】</option>
										<option ${c16}>【追加装備】</option>
										<option ${c17}>【特殊装備】</option>
										<option ${c18}>【ノーマルクリスタ】</option>
										<option ${c19}>【ウェポンクリスタ】</option>
										<option ${c20}>【アーマークリスタ】</option>
										<option ${c21}>【オプションクリスタ】</option>
										<option ${c22}>【アクセサリークリスタ】</option>
										<option ${c23}>【アドバンスクリスタ】</option>
										<option ${c24}>【開封アイテム】</option>
								</select></th>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-auto mx-auto">
						<table class="table-bordered">

							<tr class="table-secondary">
								<th style="width: 400px;" class="text-center">基礎ATK</th>
								<th style="width: 400px;" class="text-center">安定率(%)</th>
								<th style="width: 400px;" class="text-center">基礎DEF</th>
							</tr>

							<tr>
								<th class="text-center"><input type="number" name="atk"
									class="form-control" value="${item.atk }"></th>
								<th class="text-center"><input type="number"
									name="stability" class="form-control"
									value="${item.stability }"></th>
								<th class="text-center"><input type="number" name="def"
									class="form-control" value="${item.def }"></th>
							</tr>

						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-auto mx-auto">
						<table class="table-bordered">

							<tr class="table-secondary">
								<th style="width: 1200px;" class="text-center">能力(全角スペースで改行)</th>
							</tr>

							<tr>
								<th class="text-center"><input type="text" name="detail"
									class="form-control" value="${item.detail }"></th>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-auto mx-auto">
						<table class="table-bordered">

							<tr class="table-secondary">
								<th style="width: 1200px;" class="text-center">隠し能力(全角スペースで改行)</th>
							</tr>

							<tr>
								<th class="text-center"><input type="text"
									name="hiddenProperty" class="form-control"
									value="${item.hidden_property}"></th>
							</tr>

						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-auto mx-auto">
						<table class="table-bordered">

							<tr class="table-secondary">
								<th style="width: 400px;" class="text-center">染色A</th>
								<th style="width: 400px;" class="text-center">染色B</th>
								<th style="width: 400px;" class="text-center">染色C</th>
							</tr>

							<tr>
								<th class="text-center"><input type="text" name="colorA"
									class="form-control" value="${item.colorA}"></th>
								<th class="text-center"><input type="text" name="colorB"
									class="form-control" value="${item.colorB}"></th>
								<th class="text-center"><input type="text" name="colorC"
									class="form-control" value="${item.colorC}"></th>
							</tr>

						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-auto mx-auto">
						<table class="table-bordered">

							<tr class="table-secondary">
								<th style="width: 400px;" class="text-center">1s</th>
								<th style="width: 400px;" class="text-center">トレード</th>
								<th style="width: 400px;" class="text-center">売却</th>
							</tr>

							<tr>
								<th class="text-center"><input type="number" name="stack"
									class="form-control" value="${item.stack}" required></th>
								<th class="text-center">
									<div class="custom-control custom-radio">
										<input type="radio" name="trade" id="customRadio1" value=""
											class="custom-control-input" ${t0}> <label
											class="custom-control-label" for="customRadio1">可</label>
									</div>
									<div class="custom-control custom-radio">
										<input type="radio" name="trade" id="customRadio2" value="1"
											class="custom-control-input" ${t1}> <label
											class="custom-control-label" for="customRadio2">不可</label>
									</div>
								</th>
								<th class="text-center">
									<div class="custom-control custom-radio">
										<input type="radio" name="sale" id="customRadio3" value=""
											class="custom-control-input" ${s0}> <label
											class="custom-control-label" for="customRadio3">可</label>
									</div>
									<div class="custom-control custom-radio">
										<input type="radio" name="sale" id="customRadio4" value="1"
											class="custom-control-input" ${s1}> <label
											class="custom-control-label" for="customRadio4">不可</label>
									</div>
								</th>
							</tr>

						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-auto mx-auto">
						<table class="table-bordered">

							<tr class="table-secondary">
								<th style="width: 1200px;" class="text-center">備考</th>
							</tr>

							<tr>
								<th class="text-center"><input name="remarks" type="text"
									class="form-control" value="${item.remarks}"></th>
							</tr>

						</table>
					</div>
				</div>
			</c:forEach>
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 1200px;" class="text-center">削除する入手情報</th>
						</tr>

						<c:forEach var="mob" items="${mob}">
							<tr>
								<td class="text-center"><input type="checkbox"
									id="mob${mob.id }" name="mobid" value="${mob.id}"
									class="form-check-input"> <label
									class="form-check-label" for="mob${mob.id }"></label><a
									class="link text-info" href="MobdetailsServlet?id=${mob.id}">${mob.name}
										／ ${mob.map}</a></td>
							</tr>
						</c:forEach>
						<c:forEach var="boss" items="${boss}">
							<tr>
								<td class="text-center"><input type="checkbox"
									id="boss${boss.id }" name="bossid" value="${boss.id}"
									class="form-check-input"> <label
									class="form-check-label" for="boss${boss.id }"></label><a class="link text-info"
									href="BossdetailsServlet?id=${boss.id}">${boss.name} ／
										${boss.map}</a></td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-auto mx-auto">
					<button type="submit" style="width: 200px;"
						class="btn btn-secondary">更新</button>
				</div>
			</div>
		</form>
		<br>
		<div class="row">
			<div class="mx-auto">
				<a class="link text-info" href="EditMenuServlet">編集メニューに戻る</a>
			</div>
		</div>
	</div>
</body>
</html>
