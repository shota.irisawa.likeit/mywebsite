package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MobDao;
import model.ItemBeans;
import model.MobBeans;

/**
 * Servlet implementation class MobdetailsServlet
 */
@WebServlet("/MobdetailsServlet")
public class MobdetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MobdetailsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		if(request.getParameter("id") == null) {

			request.setAttribute("data", 0);
			request.setAttribute("iSearch", "checked");
			request.setAttribute("bSearch", "checked");
			request.setAttribute("mSearch", "checked");
			request.setAttribute("searchA", "checked");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);

		} else {

			try {

				String Sid = request.getParameter("id");
				int id = Integer.parseInt(Sid);

				//IDをもとにモブ情報を取得
				MobDao MobDao = new MobDao();
				List<MobBeans> mob = MobDao.searchId(id);
				List<ItemBeans> item = MobDao.mobitemsearchId(id);

				if(mob == null) {

					request.setAttribute("data", 0);
					request.setAttribute("iSearch", "checked");
					request.setAttribute("bSearch", "checked");
					request.setAttribute("mSearch", "checked");
					request.setAttribute("searchA", "checked");
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
					dispatcher.forward(request, response);

				} else {

					String[] lvArray = mob.get(0).getLv().split("/");
					String[] expArray = mob.get(0).getExp().split("/");

					List<MobBeans> lvExp = new ArrayList<MobBeans>();

					for(int i = 1; i <= lvArray.length && i <= expArray.length; i++) {

						String lv = lvArray[i - 1];
						String exp = expArray[i - 1];

						MobBeans mobLvExp = new MobBeans(lv, exp);
						lvExp.add(mobLvExp);

					}

					//モブ情報をセットし「Monster」へフォワード
					request.setAttribute("mob", mob);
					request.setAttribute("item", item);

					request.setAttribute("mobLvExp", lvExp);


					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/mobdetails.jsp");
					dispatcher.forward(request, response);

				}

			} catch(Exception e) {

				request.setAttribute("data", 0);
				request.setAttribute("iSearch", "checked");
				request.setAttribute("bSearch", "checked");
				request.setAttribute("mSearch", "checked");
				request.setAttribute("searchA", "checked");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
				dispatcher.forward(request, response);

			}

		}

	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
