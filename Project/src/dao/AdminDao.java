package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.AdminBeans;

public class AdminDao {

	//ログインIDとパスワードが一致する管理者データを検索する
	public AdminBeans findByLoginInfo(String loginId, String password) {

		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文。引数をそれぞれ代入し検索を行う
			String sql = "SELECT * FROM admin WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			//該当するデータが存在しない場合nullを戻り値として返す
			//該当するデータが存在する場合ログインIDとパスワードを戻り値として返す
			if(!rs.next()) {
				return null;
			} else {
				String lId = rs.getString("login_id");
				String pass = rs.getString("password");
				return new AdminBeans (lId, pass);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();

				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}

	}

	//管理者データの更新
	public int AdminUpdate (String loginId, String nloginId, String npass) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "UPDATE admin"
					+ " SET login_id = ?,"
						+ " password = ?"
					+ " WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, nloginId);
			pStmt.setString(2, npass);
			pStmt.setString(3, loginId);

			pStmt.executeUpdate();

			return 0;

		} catch (SQLException e) {
			e.printStackTrace();
			return 1;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

}
