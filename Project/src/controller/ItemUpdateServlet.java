package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemBossDao;
import dao.ItemDao;
import dao.ItemMobDao;
import model.BossBeans;
import model.ItemBeans;
import model.MobBeans;

/**
 * Servlet implementation class ItemUpdateServlet
 */
@WebServlet("/ItemUpdateServlet")
public class ItemUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {
			if(request.getParameter("id") == null) {

				request.setAttribute("data", 0);
				request.setAttribute("iSearch", "checked");
				request.setAttribute("bSearch", "checked");
				request.setAttribute("mSearch", "checked");
				request.setAttribute("searchA", "checked");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
				dispatcher.forward(request, response);

			} else {

				try {

					String Sid = request.getParameter("id");
					int id = Integer.parseInt(Sid);

					//IDをもとにアイテム情報を取得、データを更新。選択されたドロップデータを削除
					ItemDao itemDao = new ItemDao();
					List<ItemBeans> item = itemDao.searchIdEdit(id);
					List<MobBeans> mob = itemDao.itemmobsearchId(id);
					List<BossBeans> boss = itemDao.itembosssearchId(id);

					if(item == null) {

						request.setAttribute("data", 0);
						request.setAttribute("iSearch", "checked");
						request.setAttribute("bSearch", "checked");
						request.setAttribute("mSearch", "checked");
						request.setAttribute("searchA", "checked");
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
						dispatcher.forward(request, response);

					} else {

						//アイテム情報とドロップ情報をセットし「Item Update」へフォワード
						request.setAttribute("item", item);
						request.setAttribute("mob", mob);
						request.setAttribute("boss", boss);

						switch(item.get(0).getCategory()) {
						case "【材料/換金アイテム】":
							request.setAttribute("c1", "selected");
							break;
						case "【使用できるアイテム】":
							request.setAttribute("c2", "selected");
							break;
						case "【片手剣】":
							request.setAttribute("c3", "selected");
							break;
						case "【両手剣】":
							request.setAttribute("c4", "selected");
							break;
						case "【抜刀剣】":
							request.setAttribute("c5", "selected");
							break;
						case "【旋風槍】":
							request.setAttribute("c6", "selected");
							break;
						case "【杖】":
							request.setAttribute("c7", "selected");
							break;
						case "【魔導具】":
							request.setAttribute("c8", "selected");
							break;
						case "【手甲】":
							request.setAttribute("c9", "selected");
							break;
						case "【弓】":
							request.setAttribute("c10", "selected");
							break;
						case "【自動弓】":
							request.setAttribute("c11", "selected");
							break;
						case "【矢】":
							request.setAttribute("c12", "selected");
							break;
						case "【短剣】":
							request.setAttribute("c13", "selected");
							break;
						case "【盾】":
							request.setAttribute("c14", "selected");
							break;
						case "【体防具】":
							request.setAttribute("c15", "selected");
							break;
						case "【追加装備】":
							request.setAttribute("c16", "selected");
							break;
						case "【特殊装備】":
							request.setAttribute("c17", "selected");
							break;
						case "【ノーマルクリスタ】":
							request.setAttribute("c18", "selected");
							break;
						case "【ウェポンクリスタ】":
							request.setAttribute("c19", "selected");
							break;
						case "【アーマークリスタ】":
							request.setAttribute("c20", "selected");
							break;
						case "【オプションクリスタ】":
							request.setAttribute("c21", "selected");
							break;
						case "【アクセサリークリスタ】":
							request.setAttribute("c22", "selected");
							break;
						case "【アドバンスクリスタ】":
							request.setAttribute("c23", "selected");
							break;
						case "【開封アイテム】":
							request.setAttribute("c24", "selected");
							break;
						}

						if(item.get(0).getCannot_trade().equals("可")) {
							request.setAttribute("t0", "checked");
						} else {
							request.setAttribute("t1", "checked");
						}

						if(item.get(0).getCannot_sale().equals("可")) {
							request.setAttribute("s0", "checked");
						} else {
							request.setAttribute("s1", "checked");
						}

						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemupdate.jsp");
						dispatcher.forward(request, response);
					}

				} catch (Exception e) {

					request.setAttribute("data", 0);
					request.setAttribute("iSearch", "checked");
					request.setAttribute("bSearch", "checked");
					request.setAttribute("mSearch", "checked");
					request.setAttribute("searchA", "checked");
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editindex.jsp");
					dispatcher.forward(request, response);

				}
			}
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {
			//文字化け防止
			request.setCharacterEncoding("UTF-8");

			//フォームの入力情報を取得
			String id = request.getParameter("id");
			String name = request.getParameter("name");
			String category = request.getParameter("category");
			String atk = request.getParameter("atk");
			String stability = request.getParameter("stability");
			String def = request.getParameter("def");
			String detail = request.getParameter("detail");
			String hiddenProperty = request.getParameter("hiddenProperty");
			String colorA = request.getParameter("colorA");
			String colorB = request.getParameter("colorB");
			String colorC = request.getParameter("colorC");
			String stack = request.getParameter("stack");
			String trade = request.getParameter("trade");
			String sale = request.getParameter("sale");
			String remarks = request.getParameter("remarks");

			int msgA = 0;
			int msgB = 0;
			int drop = 0;

			if(request.getParameterValues("bossid") != null) {

				String[] bId = request.getParameterValues("bossid");
				for(int i = 1; i <= bId.length; i++) {
					String iId = request.getParameter("id");
					int item_id = Integer.parseInt(iId);

					String bossid = bId[bId.length - i];
					int boss_id = Integer.parseInt(bossid);

					dao.ItemBossDao ItemBossDao = new ItemBossDao();

					msgA = msgA + ItemBossDao.bossDropDelete(boss_id, item_id);
					drop = drop + 1;

				}
			}

			if(request.getParameterValues("mobid") != null) {

				String[] mId = request.getParameterValues("mobid");
				for(int j = 1; j <= mId.length; j++) {
					String iId = request.getParameter("id");
					int item_id = Integer.parseInt(iId);

					String mobid = mId[mId.length - j];
					int mob_id = Integer.parseInt(mobid);

					dao.ItemMobDao ItemMobDao = new ItemMobDao();

					msgA = msgA + ItemMobDao.mobDropDelete(mob_id, item_id);
					drop = drop + 1;

				}
			}

			ItemDao itemDao = new ItemDao();
			msgB = itemDao.itemUpdate(name, category, atk, stability, def, detail, hiddenProperty, colorA, colorB, colorC, stack, trade, sale, remarks, id);

			//メッセージをセットし「Edit Menu」へフォワード
			String msg;
			if(msgA * msgB == 0) {
				if(drop == 0) {
					msg = "アイテムデータを1件更新しました";
				} else {
					msg =  "アイテムデータを1件更新、ドロップデータを" + drop + "件削除しました";
				}
				request.setAttribute("msg", msg);
			} else {
				request.setAttribute("msg2", "エラーが発生しました");
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editmenu.jsp");
			dispatcher.forward(request, response);
		}
	}
}
