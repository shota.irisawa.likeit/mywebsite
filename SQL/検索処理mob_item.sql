﻿USE myWebsite_db;

SELECT
	mob_data.name, mob_data.map, item_data.name
FROM
	mob_data
JOIN
	item_and_mob
ON
	mob_data.id = item_and_mob.mob_id
JOIN
	item_data
ON
	item_and_mob.item_id = item_data.id
WHERE
	mob_data.name LIKE '%の%';
	 
