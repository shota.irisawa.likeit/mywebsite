﻿USE myWebsite_db;

SELECT
	boss_data.name, boss_data.map, item_data.name
FROM
	boss_data
JOIN
	item_and_boss
ON
	boss_data.id = item_and_boss.boss_id
JOIN
	item_data
ON
	item_and_boss.item_id = item_data.id
WHERE
	boss_data.name LIKE '%の%';
	 
