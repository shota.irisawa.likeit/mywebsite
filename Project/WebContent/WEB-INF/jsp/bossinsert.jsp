<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>【Boss Insert】</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"></li>
		</ul>
		<ul class="navbar-nav">
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link text-danger" href="LogoutServlet">ログアウト</a></li>
		</ul>
	</nav>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-auto mx-auto">
				<h2>
					<strong>Boss Insert</strong>
				</h2>
			</div>
		</div>
		<br>
		<form action="BossInsertServlet" method="post">
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 1200px;" class="text-center">モンスター名</th>
						</tr>

						<tr>
							<th class="text-center"><input type="text" name="name"
								class="form-control" required></th>
						</tr>

					</table>
				</div>
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 1200px;" class="text-center">マップ</th>
						</tr>

						<tr>
							<th class="text-center"><input type="text" name="map"
								class="form-control" required></th>
						</tr>

					</table>
				</div>
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 1200px;" class="text-center">属性</th>
						</tr>

						<tr>
							<th class="text-center"><select class="form-control"
								name="element" id="sel1">
									<option selected>無属性</option>
									<option>火属性</option>
									<option>地属性</option>
									<option>風属性</option>
									<option>水属性</option>
									<option>光属性</option>
									<option>闇属性</option>
							</select></th>
						</tr>

					</table>
				</div>
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 1200px;" class="text-center">難易度選択</th>
						</tr>

						<tr>
							<th class="text-center">
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadio1" name="difficulty"
										value="1" class="custom-control-input" checked> <label
										class="custom-control-label" for="customRadio1">可</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadio2" name="difficulty"
										value="0" class="custom-control-input"> <label
										class="custom-control-label" for="customRadio2">不可</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadio3" name="difficulty"
										value="2" class="custom-control-input"> <label
										class="custom-control-label" for="customRadio3">Easy不可</label>
								</div>
							</th>
						</tr>

					</table>
				</div>

				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 400px;" class="text-center">難易度</th>
							<th style="width: 400px;" class="text-center">Lv</th>
							<th style="width: 400px;" class="text-center">最大HP(不定の場合は0と入力)</th>
							<th style="width: 400px;" class="text-center">経験値</th>
						</tr>
						<tr>
							<th class="text-center">Normal</th>
							<th class="text-center"><input type="number" name="lv"
								class="form-control" required></th>
							<th class="text-center"><input type="number" name="hp"
								class="form-control" required></th>
							<th class="text-center"><input type="number" name="exp"
								class="form-control" required></th>
						</tr>
					</table>
				</div>
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 1200px;" class="text-center">部位破壊(箇所)</th>
						</tr>

						<tr>
							<th class="text-center"><input type="number" name="bbreak"
								class="form-control" required></th>
						</tr>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 1200px;" class="text-center">備考</th>
						</tr>

						<tr>
							<th class="text-center"><input type="text" name="remarks"
								class="form-control"></th>
						</tr>

					</table>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-auto mx-auto">
					<button type="submit" style="width: 200px;"
						class="btn btn-secondary">登録</button>
				</div>
			</div>
		</form>
		<br>

		<div class="row">
			<div class="mx-auto">
				<a class="link text-info" href="EditMenuServlet">編集メニューに戻る</a>
			</div>
		</div>
	</div>
</body>
</html>
