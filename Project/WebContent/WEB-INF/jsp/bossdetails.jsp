<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:forEach var="boss" items="${boss}">
	<title>【Boss Data】 Lv.${boss.lv} ${boss.name}</title>
</c:forEach>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"></li>
		</ul>
		<ul class="navbar-nav">
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link text-danger" href="EditMenuServlet">編集</a></li>
		</ul>
	</nav>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-auto mx-auto">
				<h2>
					<strong>Boss Data</strong>
				</h2>
			</div>
		</div>
	</div>

	<br>
	<c:forEach var="boss" items="${boss}">
		<div class="row">
			<div class="col-auto mx-auto">
				<table class="table-bordered">

					<tr class="table-secondary">
						<td style="width: 1200px;" class="text-center">モンスター名</td>
					</tr>

					<tr>
						<td class="text-center">${boss.name}</td>
					</tr>

				</table>
			</div>
		</div>

		<div class="row">

			<div class="col-auto mx-auto">
				<table class="table-bordered">

					<tr class="table-secondary">
						<td style="width: 1200px;" class="text-center">マップ</td>
					</tr>

					<tr>
						<td class="text-center">${boss.map}</td>
					</tr>

				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-auto mx-auto">
				<table class="table-bordered">

					<tr class="table-secondary">
						<td style="width: 1200px;" class="text-center">属性</td>
					</tr>

					<tr>
						<td class="text-center">${boss.element}</td>
					</tr>

				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-auto mx-auto">
				<table class="table-bordered">

					<tr class="table-secondary">
						<td style="width: 300px;" class="text-center">難易度</td>
						<td style="width: 300px;" class="text-center">Lv</td>
						<td style="width: 300px;" class="text-center">HP</td>
						<td style="width: 300px;" class="text-center">基礎経験値</td>
					</tr>

					<c:if test="${boss.difficulty == 1}">
						<tr>
							<td class="text-center">Easy</td>
							<c:if test="${(boss.lv - 10) <= 0}">
								<td class="text-center">1</td>
								<c:if test="${boss.hp <= 0}">
									<td class="text-center">???</td>
								</c:if>
								<c:if test="${boss.hp > 0}">
									<td class="text-center">${Math.round(boss.hp / 10 - 0.5)}</td>
								</c:if>
								<td class="text-center">${Math.round(boss.exp / 10 - 0.5)}</td>
							</c:if>
							<c:if test="${(boss.lv - 10) > 0}">
								<td class="text-center">${boss.lv - 10}</td>
								<c:if test="${boss.hp <= 0}">
									<td class="text-center">???</td>
								</c:if>
								<c:if test="${boss.hp > 0}">
									<td class="text-center">${Math.round(boss.hp / 10 - 0.5)}</td>
								</c:if>
								<td class="text-center">${Math.round(boss.exp / 10 - 0.5)}</td>
							</c:if>
						</tr>
						<tr>
							<td class="text-center">Normal</td>
							<td class="text-center">${boss.lv}</td>
							<c:if test="${boss.hp <= 0}">
								<td class="text-center">???</td>
							</c:if>
							<c:if test="${boss.hp > 0}">
								<td class="text-center">${boss.hp}</td>
							</c:if>
							<td class="text-center">${boss.exp}</td>
						</tr>
						<tr>
							<td class="text-center">Hard</td>
							<td class="text-center">${boss.lv + 10}</td>
							<c:if test="${boss.hp <= 0}">
								<td class="text-center">???</td>
							</c:if>
							<c:if test="${boss.hp > 0}">
								<td class="text-center">${boss.hp * 2}</td>
							</c:if>
							<td class="text-center">${boss.exp * 2}</td>
						</tr>
						<tr>
							<td class="text-center">Lunatic</td>
							<td class="text-center">${boss.lv + 20}</td>
							<c:if test="${boss.hp <= 0}">
								<td class="text-center">???</td>
							</c:if>
							<c:if test="${boss.hp > 0}">
								<td class="text-center">${boss.hp * 5}</td>
							</c:if>
							<td class="text-center">${boss.exp * 5}</td>
						</tr>
						<tr>
							<td class="text-center">Ultimate</td>
							<td class="text-center">${boss.lv + 40}</td>
							<c:if test="${boss.hp <= 0}">
								<td class="text-center">???</td>
							</c:if>
							<c:if test="${boss.hp > 0}">
								<td class="text-center">${boss.hp * 10}</td>
							</c:if>
							<td class="text-center">${boss.exp * 10}</td>
						</tr>
					</c:if>
					<c:if test="${boss.difficulty == 2}">
						<tr>
							<td class="text-center">Normal</td>
							<td class="text-center">${boss.lv}</td>
							<td class="text-center">${boss.hp}</td>
							<td class="text-center">${boss.exp}</td>
						</tr>
						<tr>
							<td class="text-center">Hard</td>
							<td class="text-center">${boss.lv + 10}</td>
							<td class="text-center">${boss.hp * 2}</td>
							<td class="text-center">${boss.exp * 2}</td>
						</tr>
						<tr>
							<td class="text-center">Lunatic</td>
							<td class="text-center">${boss.lv + 20}</td>
							<td class="text-center">${boss.hp * 5}</td>
							<td class="text-center">${boss.exp * 5}</td>
						</tr>
						<tr>
							<td class="text-center">Ultimate</td>
							<td class="text-center">${boss.lv + 40}</td>
							<td class="text-center">${boss.hp * 10}</td>
							<td class="text-center">${boss.exp * 10}</td>
						</tr>
					</c:if>
					<c:if test="${boss.difficulty == 0}">
						<tr>
							<td class="text-center">-</td>
							<td class="text-center">${boss.lv}</td>
							<td class="text-center">${boss.hp}</td>
							<td class="text-center">${boss.exp}</td>
						</tr>
					</c:if>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-auto mx-auto">
				<table class="table-bordered">

					<tr class="table-secondary">
						<td style="width: 1200px;" class="text-center">部位破壊</td>
					</tr>

					<tr>
						<td class="text-center">${boss.bbreak}箇所</td>
					</tr>
				</table>
			</div>
		</div>

	</c:forEach>


	<div class="row">
		<div class="col-auto mx-auto">
			<table class="table-bordered">

				<tr class="table-secondary">
					<td style="width: 1200px;" class="text-center">ドロップアイテム</td>
				</tr>
				<c:forEach var="item" items="${item}">
					<tr>
						<td class="text-center"><a class="link text-info"
							href="ItemdetailsServlet?id=${item.id}">${item.category} ${item.name}</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>

	<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<a class="link text-info" href="IndexServlet">TOPへ戻る</a>
		</div>
	</div>
</body>
</html>
