package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemDao;

/**
 * Servlet implementation class ItemInsertServlet
 */
@WebServlet("/ItemInsertServlet")
public class ItemInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemInsertServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/iteminsert.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {
			//文字化け防止
			request.setCharacterEncoding("UTF-8");

			//フォームの入力情報を取得、データを新規登録
			String name = request.getParameter("name");
			String category = request.getParameter("category");
			String atk = request.getParameter("atk");
			String stability = request.getParameter("stability");
			String def = request.getParameter("def");
			String detail = request.getParameter("detail");
			String hiddenProperty = request.getParameter("hiddenProperty");
			String colorA = request.getParameter("colorA");
			String colorB = request.getParameter("colorB");
			String colorC = request.getParameter("colorC");
			String stack = request.getParameter("stack");
			String trade = request.getParameter("trade");
			String sale = request.getParameter("sale");
			String remarks = request.getParameter("remarks");

			int msg = 0;

			ItemDao itemDao = new ItemDao();
			msg = itemDao.itemInsert(name, category, atk, stability, def, detail, hiddenProperty, colorA, colorB, colorC, stack, trade, sale, remarks);

			//メッセージをセットし「Edit Menu」へフォワード
			if(msg == 0) {
				request.setAttribute("msg", "アイテムデータを1件登録しました");
			} else {
				request.setAttribute("msg2", "エラーが発生しました");
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editmenu.jsp");
			dispatcher.forward(request, response);
		}
	}

}
