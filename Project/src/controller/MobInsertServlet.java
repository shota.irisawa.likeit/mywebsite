package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.MobDao;

/**
 * Servlet implementation class MobInsertServlet
 */
@WebServlet("/MobInsertServlet")
public class MobInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MobInsertServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/mobinsert.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {
			//文字化け防止
			request.setCharacterEncoding("UTF-8");

			//フォームの入力情報を取得、データを新規登録
			String name = request.getParameter("name");
			String map = request.getParameter("map");
			String category = request.getParameter("category");
			String element = request.getParameter("element");
			String lv = request.getParameter("lv");
			String exp = request.getParameter("exp");
			String remarks = request.getParameter("remarks");

			int msg = 0;

			MobDao MobDao = new MobDao();

			msg = MobDao.MobInsert(name, map, category, element, lv, exp, remarks);

			//メッセージをセットし「Edit Menu」へフォワード
			if(msg == 0) {
				request.setAttribute("msg", "モンスターデータを1件登録しました");
			} else {
				request.setAttribute("msg2", "エラーが発生しました");
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editmenu.jsp");
			dispatcher.forward(request, response);
		}
	}

}
