package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.BossBeans;
import model.ItemBeans;
import model.MobBeans;

public class ItemDao {

	//検索ワードをもとにアイテムデータを検索
	//該当データをすべてList型のコレクションで返す
	public List<ItemBeans> search (String[] searchWords, String searchOption) {

		Connection conn = null;
		List<ItemBeans> itemList = new ArrayList<ItemBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;
			String word = "WHERE (item_data.name LIKE CONCAT('%', ?, '%')"
					+ " OR item_data.detail LIKE CONCAT('%', ?, '%')"
					+ " OR item_data.category LIKE CONCAT('%', ?, '%')"
					+ " OR item_data.hidden_property LIKE CONCAT('%', ?, '%')"
					+ " OR item_data.remarks LIKE CONCAT('%', ?, '%'))";

			for(int i = 2; i <= searchWords.length; i = i + 1) {
				word = word + " " + searchOption + " (item_data.name LIKE CONCAT('%', ?, '%')"
						+ " OR item_data.detail LIKE CONCAT('%', ?, '%')"
						+ " OR item_data.category LIKE CONCAT('%', ?, '%')"
						+ " OR item_data.hidden_property LIKE CONCAT('%', ?, '%')"
						+ " OR item_data.remarks LIKE CONCAT('%', ?, '%'))";
			}

			sql = "SELECT item_data.name, item_data.id, item_data.category, item_data.remarks"
					+ " FROM item_data " + word
					+ " ORDER BY item_data.name";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			for(int j = 1; j <= searchWords.length; j = j + 1) {

				pStmt.setString((5 * j) - 4, searchWords[j - 1]);
				pStmt.setString((5 * j) - 3, searchWords[j - 1]);
				pStmt.setString((5 * j) - 2, searchWords[j - 1]);
				pStmt.setString((5 * j) - 1, searchWords[j - 1]);
				pStmt.setString((5 * j), searchWords[j - 1]);

			}



			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String category = rs.getString("category");
				String remarks = rs.getString("remarks");
				if(remarks.length() != 0) {
					remarks = "<br>[" + remarks + "]";
				}
				ItemBeans Item = new ItemBeans(id, name, category, remarks);

				itemList.add(Item);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return itemList;

	}

	//IDをもとにアイテムデータを検索
	//該当データの詳細をList型のコレクションで返す
	public List<ItemBeans> searchId (int id) {

		Connection conn = null;
		List<ItemBeans> item = new ArrayList<ItemBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "SELECT * FROM item_data WHERE item_data.id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {
				String name = rs.getString("name");
				String category = rs.getString("category");
				String def = rs.getString("def");
				String atk = rs.getString("atk");
				String stability = rs.getString("stability");
				String detail = rs.getString("detail");
				String colorA = rs.getString("colorA");
				String colorB = rs.getString("colorB");
				String colorC = rs.getString("colorC");
				String stack = rs.getString("stack");
				String cannot_trade = rs.getString("cannot_trade");
				String cannot_sale = rs.getString("cannot_sale");

				String remarks = rs.getString("remarks");

				String[] d = detail.split("　");
				String ndetail = d[0];

				for(int i = 1; i <= d.length - 1; i++) {

					ndetail = ndetail + "<br>" + d[i];
				}


				String trade = "";
				String sale = "";

				if(cannot_trade.equals("1")) {
					trade = "不可";
				} else {
					trade = "可";
				}

				if(cannot_sale.equals("1")) {
					sale = "不可";
				} else {
					sale = "可";
				}

				String hidden_property = rs.getString("hidden_property");

				String[] h = hidden_property.split("　");
				String nhidden = h[0];

				for(int j = 1; j <= h.length - 1; j++) {

					nhidden = nhidden + "<br>" + h[j];

				}


				ItemBeans itemDetail = new ItemBeans(id, name, category, def, atk, stability, ndetail, colorA, colorB, colorC, stack, trade, sale, nhidden, remarks);

				item.add(itemDetail);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return item;

	}

	//IDをもとにアイテムデータを検索。編集用
	//該当データの詳細をList型のコレクションで返す
	public List<ItemBeans> searchIdEdit (int id) {

		Connection conn = null;
		List<ItemBeans> item = new ArrayList<ItemBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "SELECT * FROM item_data WHERE item_data.id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {
				String name = rs.getString("name");
				String category = rs.getString("category");
				String def = rs.getString("def");
				String atk = rs.getString("atk");
				String stability = rs.getString("stability");
				String detail = rs.getString("detail");
				String colorA = rs.getString("colorA");
				String colorB = rs.getString("colorB");
				String colorC = rs.getString("colorC");
				String stack = rs.getString("stack");
				String cannot_trade = rs.getString("cannot_trade");
				String cannot_sale = rs.getString("cannot_sale");

				String remarks = rs.getString("remarks");

				String trade = "";
				String sale = "";

				if(cannot_trade.equals("1")) {
					trade = "不可";
				} else {
					trade = "可";
				}

				if(cannot_sale.equals("1")) {
					sale = "不可";
				} else {
					sale = "可";
				}

				String hidden_property = rs.getString("hidden_property");

				ItemBeans itemDetail = new ItemBeans(id, name, category, def, atk, stability, detail, colorA, colorB, colorC, stack, trade, sale, hidden_property, remarks);

				item.add(itemDetail);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return item;

	}

	//アイテムIDをもとにアイテムをドロップするモブのIDと名前を検索
	//該当データをList型のコレクションで返す
	public List<MobBeans> itemmobsearchId (int id) {

		Connection conn = null;
		List<MobBeans> mob = new ArrayList<MobBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "SELECT mob_data.id, mob_data.name, mob_data.map"
					+ " FROM item_data JOIN item_and_mob"
					+ " ON item_data.id = item_and_mob.item_id"
					+ " JOIN mob_data"
					+ " ON mob_data.id = item_and_mob.mob_id"
					+ " WHERE item_data.id = ?"
					+ " ORDER BY mob_data.id";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int mobId = rs.getInt("id");
				String mobName = rs.getString("name");
				String mobMap = rs.getString("map");

				MobBeans mobDetail = new MobBeans(mobId, mobName, mobMap, "");

				mob.add(mobDetail);

			}



		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return mob;

	}

	//アイテムIDをもとにアイテムをドロップするボスのIDと名前を検索
	//該当データをList型のコレクションで返す
	public List<BossBeans> itembosssearchId (int id) {

		Connection conn = null;
		List<BossBeans> boss = new ArrayList<BossBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "SELECT boss_data.id, boss_data.name, boss_data.map"
					+ " FROM item_data"
					+ " JOIN item_and_boss"
					+ " ON item_data.id = item_and_boss.item_id"
					+ " JOIN boss_data"
					+ " ON boss_data.id = item_and_boss.boss_id"
					+ " WHERE item_data.id = ?"
					+ " ORDER BY boss_data.id";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int bossId = rs.getInt("id");
				String bossName = rs.getString("name");
				String bossMap = rs.getString("map");

				BossBeans bossDetail = new BossBeans(bossId, bossName, bossMap, "");

				boss.add(bossDetail);

			}



		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return boss;

	}

	//アイテムデータの新規登録
	public int itemInsert (String name, String category, String atk, String stability, String def, String detail
			, String hiddenProperty, String colorA, String colorB, String colorC, String stack, String trade, String sale, String remarks) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "INSERT INTO item_data (name, category, def, atk, stability, detail, colorA, colorB, colorC, stack, cannot_trade, cannot_sale, hidden_property, remarks)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, category);
			pStmt.setString(3, def);
			pStmt.setString(4, atk);
			pStmt.setString(5, stability);
			pStmt.setString(6, detail);
			pStmt.setString(7, colorA);
			pStmt.setString(8, colorB);
			pStmt.setString(9, colorC);
			pStmt.setString(10, stack);
			pStmt.setString(11, trade);
			pStmt.setString(12, sale);
			pStmt.setString(13, hiddenProperty);
			pStmt.setString(14, remarks);

			pStmt.executeUpdate();

			return 0;

		} catch (SQLException e) {
			e.printStackTrace();
			return 1;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	//最新30件のアイテムデータをすべてList型のコレクションで返す
	public List<ItemBeans> all () {

		Connection conn = null;
		List<ItemBeans> itemList = new ArrayList<ItemBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql = "SELECT item_data.id, item_data.name, item_data.remarks"
					+ " FROM item_data"
					+ " ORDER BY item_data.id DESC"
					+ " LIMIT 30";

			Statement Stmt = conn.createStatement();

			ResultSet rs = Stmt.executeQuery(sql);

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String remarks = rs.getString("remarks");
				if(remarks.length() != 0) {
					remarks = "<br>[" + remarks + "]";
				}
				ItemBeans Item = new ItemBeans(id, name, "",remarks);

				itemList.add(Item);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return itemList;

	}

	//検索ワードをもとにアイテムデータを検索。編集用
	//該当データをすべてList型のコレクションで返す
	public List<ItemBeans> searchE (String[] searchWords, String searchOption) {

		Connection conn = null;
		List<ItemBeans> itemList = new ArrayList<ItemBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;
			String word = "WHERE (item_data.name = ? OR item_data.remarks = ?)";

			for(int i = 2; i <= searchWords.length; i = i + 1) {
				word = word + " " + searchOption + " (item_data.name = ? OR item_data.remarks = ?)";
			}

			sql = "SELECT item_data.name, item_data.id, item_data.remarks"
					+ " FROM item_data"
					+ " " + word
					+ " ORDER BY item_data.id DESC";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			for(int j = 1; j <= searchWords.length; j = j + 1) {

				pStmt.setString((2 * j) - 1, searchWords[j - 1]);
				pStmt.setString((2 * j), searchWords[j - 1]);

			}



			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String remarks = rs.getString("remarks");
				if(remarks.length() != 0) {
					remarks = "<br>[" + remarks + "]";
				}
				ItemBeans Item = new ItemBeans(id, name, "", remarks);

				itemList.add(Item);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return itemList;

	}

	//アイテムデータの更新
	public int itemUpdate (String name, String category, String atk, String stability, String def, String detail
			, String hiddenProperty, String colorA, String colorB, String colorC, String stack, String trade, String sale, String remarks, String id) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "UPDATE item_data"
					+ " SET name = ?,"
						+ " category = ?,"
						+ " def = ?,"
						+ " atk = ?,"
						+ " stability = ?,"
						+ " detail = ?,"
						+ " colorA = ?,"
						+ " colorB = ?,"
						+ " colorC = ?,"
						+ " stack = ?,"
						+ " cannot_trade = ?,"
						+ " cannot_sale = ?,"
						+ " hidden_property = ?,"
						+ " remarks = ?"
					+ " WHERE"
						+ " item_data.id = ?"
					+ " LIMIT 1";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, category);
			pStmt.setString(3, def);
			pStmt.setString(4, atk);
			pStmt.setString(5, stability);
			pStmt.setString(6, detail);
			pStmt.setString(7, colorA);
			pStmt.setString(8, colorB);
			pStmt.setString(9, colorC);
			pStmt.setString(10, stack);
			pStmt.setString(11, trade);
			pStmt.setString(12, sale);
			pStmt.setString(13, hiddenProperty);
			pStmt.setString(14, remarks);
			pStmt.setString(15, id);

			pStmt.executeUpdate();

			return 0;

		} catch (SQLException e) {
			e.printStackTrace();
			return 1;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	//アイテムデータの削除
	public int ItemDelete (int id) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "DELETE FROM item_data"
					+ " WHERE id = ?"
					+ " LIMIT 1";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			pStmt.executeUpdate();

			return 0;

		} catch (SQLException e) {
			e.printStackTrace();
			return 1;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

}
