package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BossDao;
import model.BossBeans;
import model.ItemBeans;

/**
 * Servlet implementation class BossdetailsServlet
 */
@WebServlet("/BossdetailsServlet")
public class BossdetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BossdetailsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		if(request.getParameter("id") == null) {

			request.setAttribute("data", 0);
			request.setAttribute("iSearch", "checked");
			request.setAttribute("bSearch", "checked");
			request.setAttribute("mSearch", "checked");
			request.setAttribute("searchA", "checked");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);

		} else {

			try {

				String Sid = request.getParameter("id");
				int id = Integer.parseInt(Sid);

				//IDをもとにボス情報を取得
				BossDao BossDao = new BossDao();
				List<BossBeans> boss = BossDao.searchId(id);
				List<ItemBeans> item = BossDao.bossitemsearchId(id);

				if(boss == null) {

					request.setAttribute("data", 0);
					request.setAttribute("iSearch", "checked");
					request.setAttribute("bSearch", "checked");
					request.setAttribute("mSearch", "checked");
					request.setAttribute("searchA", "checked");
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
					dispatcher.forward(request, response);

				} else {

					//ボス情報をセットし「Boss」へフォワード
					request.setAttribute("boss", boss);
					request.setAttribute("item", item);

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bossdetails.jsp");
					dispatcher.forward(request, response);

				}

			} catch (Exception e) {

				request.setAttribute("data", 0);
				request.setAttribute("iSearch", "checked");
				request.setAttribute("bSearch", "checked");
				request.setAttribute("mSearch", "checked");
				request.setAttribute("searchA", "checked");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
				dispatcher.forward(request, response);

			}

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
