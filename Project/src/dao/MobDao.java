package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.ItemBeans;
import model.MobBeans;

public class MobDao {

	//IDをもとにモブデータを検索
	//該当データの詳細をList型のコレクションで返す
	public List<MobBeans> searchId (int id) {

		Connection conn = null;
		List<MobBeans> mob = new ArrayList<MobBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "SELECT * FROM mob_data WHERE mob_data.id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {

				String name = rs.getString("name");
				String map = rs.getString("map");
				String element = rs.getString("element");
				String lv = rs.getString("lv");
				String exp = rs.getString("exp");
				String category = rs.getString("category");
				String remarks = rs.getString("remarks");

				MobBeans mobDetail = new MobBeans(id, name, map, lv, exp, element, category, remarks);

				mob.add(mobDetail);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return mob;

	}


	//モブIDをもとにドロップするアイテムのIDと名前を検索
	//該当データをList型のコレクションで返す
	public List<ItemBeans> mobitemsearchId (int id) {

		Connection conn = null;
		List<ItemBeans> item = new ArrayList<ItemBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "SELECT item_data.id, item_data.name, item_data.category"
					+ " FROM item_data"
					+ " JOIN item_and_mob"
					+ " ON item_data.id = item_and_mob.item_id"
					+ " JOIN mob_data"
					+ " ON item_and_mob.mob_id = mob_data.id"
					+ " WHERE mob_data.id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int itemId = rs.getInt("id");
				String itemName = rs.getString("name");
				String itemCategory = rs.getString("category");

				ItemBeans itemDetail = new ItemBeans(itemId, itemName, itemCategory, "");

				item.add(itemDetail);

			}


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return item;

	}

	//検索ワードをもとにモブデータを検索
	//該当データをすべてList型のコレクションで返す
	public List<MobBeans> search (String[] searchWords, String searchOption) {

		Connection conn = null;
		List<MobBeans> mobList = new ArrayList<MobBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;
			String word = "WHERE (mob_data.name LIKE CONCAT('%', ?, '%')"
					+ " OR mob_data.map LIKE CONCAT('%', ?, '%')"
					+ " OR mob_data.element LIKE CONCAT('%', ?, '%'))";

			for(int i = 2; i <= searchWords.length; i = i + 1) {
				word = word + " " + searchOption + " (mob_data.name LIKE CONCAT('%', ?, '%')"
						+ " OR mob_data.map LIKE CONCAT('%', ?, '%')"
						+ " OR mob_data.element LIKE CONCAT('%', ?, '%'))";
			}

			sql = "SELECT mob_data.name, mob_data.id, mob_data.map, mob_data.category"
					+ " FROM mob_data"
					+ " " + word
					+ " ORDER BY mob_data.name";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			for(int j = 1; j <= searchWords.length; j = j + 1) {

				pStmt.setString((3 * j) - 2, searchWords[j - 1]);
				pStmt.setString((3 * j) - 1, searchWords[j - 1]);
				pStmt.setString((3 * j), searchWords[j - 1]);

			}

			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String map = rs.getString("map");
				String category = rs.getString("category");
				MobBeans mob = new MobBeans(id, name, map, category);

				mobList.add(mob);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return mobList;

	}

	//モブデータの新規登録
	public int MobInsert (String name, String map, String category, String element, String lv, String exp, String remarks) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "INSERT INTO mob_data (name, map, lv, exp, element, category, remarks)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, map);
			pStmt.setString(3, lv);
			pStmt.setString(4, exp);
			pStmt.setString(5, element);
			pStmt.setString(6, category);
			pStmt.setString(7, remarks);

			pStmt.executeUpdate();

			return 0;

		} catch (SQLException e) {
			e.printStackTrace();
			return 1;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	//最新30件のモンスターデータをすべてList型のコレクションで返す
	public List<MobBeans> all () {

		Connection conn = null;
		List<MobBeans> mobList = new ArrayList<MobBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql = "SELECT mob_data.id, mob_data.name, mob_data.remarks"
					+ " FROM mob_data"
					+ " ORDER BY mob_data.id DESC"
					+ " LIMIT 30";

			Statement Stmt = conn.createStatement();

			ResultSet rs = Stmt.executeQuery(sql);

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String remarks = rs.getString("remarks");
				if(remarks.length() != 0) {
					remarks = "<br>[" + remarks + "]";
				}
				MobBeans Mob = new MobBeans(id, name, remarks);

				mobList.add(Mob);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return mobList;

	}

	//モブデータの更新
	public int MobUpdate (String name, String map, String category, String element, String lv, String exp, String remarks, String id) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "UPDATE mob_data"
					+ " SET name = ?,"
					    + " map = ?,"
					    + " lv = ?,"
					    + " exp = ?,"
					    + " element = ?,"
					    + " category = ?,"
					    + " remarks = ?"
					+ " WHERE"
						+ " mob_data.id = ?"
					+ " LIMIT 1";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, map);
			pStmt.setString(3, lv);
			pStmt.setString(4, exp);
			pStmt.setString(5, element);
			pStmt.setString(6, category);
			pStmt.setString(7, remarks);
			pStmt.setString(8, id);

			pStmt.executeUpdate();

			return 0;

		} catch (SQLException e) {
			e.printStackTrace();
			return 1;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	//モブデータの削除
	public int MobDelete (int id) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "DELETE FROM mob_data"
					+ " WHERE id = ?"
					+ " LIMIT 1";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			pStmt.executeUpdate();

			return 0;

		} catch (SQLException e) {
			e.printStackTrace();
			return 1;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

}
