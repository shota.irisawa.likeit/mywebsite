<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>【Item Insert】</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"></li>
		</ul>
		<ul class="navbar-nav">
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link text-danger" href="LogoutServlet">ログアウト</a></li>
		</ul>
	</nav>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-auto mx-auto">
				<h2>
					<strong>Item Insert</strong>
				</h2>
			</div>
		</div>
		<br>
		<form action="ItemInsertServlet" method="post">
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 1200px;" class="text-center">アイテム名</th>
						</tr>

						<tr>
							<th class="text-center"><input type="text" name="name"
								class="form-control" required></th>
						</tr>

					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 1200px;" class="text-center">カテゴリー</th>
						</tr>

						<tr>
							<th class="text-center"><select class="form-control"
								name="category" id="sel1">
									<option selected>【材料/換金アイテム】</option>
									<option>【使用できるアイテム】</option>
									<option>【片手剣】</option>
									<option>【両手剣】</option>
									<option>【抜刀剣】</option>
									<option>【旋風槍】</option>
									<option>【杖】</option>
									<option>【魔導具】</option>
									<option>【手甲】</option>
									<option>【弓】</option>
									<option>【自動弓】</option>
									<option>【矢】</option>
									<option>【短剣】</option>
									<option>【盾】</option>
									<option>【体防具】</option>
									<option>【追加装備】</option>
									<option>【特殊装備】</option>
									<option>【ノーマルクリスタ】</option>
									<option>【ウェポンクリスタ】</option>
									<option>【アーマークリスタ】</option>
									<option>【オプションクリスタ】</option>
									<option>【アクセサリークリスタ】</option>
									<option>【アドバンスクリスタ】</option>
									<option>【開封アイテム】</option>
							</select></th>
						</tr>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 400px;" class="text-center">基礎ATK</th>
							<th style="width: 400px;" class="text-center">安定率(%)</th>
							<th style="width: 400px;" class="text-center">基礎DEF</th>
						</tr>

						<tr>
							<th class="text-center"><input type="number" name="atk"
								class="form-control"></th>
							<th class="text-center"><input type="number" name="stability"
								class="form-control"></th>
							<th class="text-center"><input type="number" name="def"
								class="form-control"></th>
						</tr>

					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 1200px;" class="text-center">能力(全角スペースで改行)</th>
						</tr>

						<tr>
							<th class="text-center"><input type="text" name="detail"
								class="form-control"></th>
						</tr>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 1200px;" class="text-center">隠し能力(全角スペースで改行)</th>
						</tr>

						<tr>
							<th class="text-center"><input type="text"
								name="hiddenProperty" class="form-control"></th>
						</tr>

					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 400px;" class="text-center">染色A</th>
							<th style="width: 400px;" class="text-center">染色B</th>
							<th style="width: 400px;" class="text-center">染色C</th>
						</tr>

						<tr>
							<th class="text-center"><input type="text" name="colorA"
								class="form-control"></th>
							<th class="text-center"><input type="text" name="colorB"
								class="form-control"></th>
							<th class="text-center"><input type="text" name="colorC"
								class="form-control"></th>
						</tr>

					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 400px;" class="text-center">1s</th>
							<th style="width: 400px;" class="text-center">トレード</th>
							<th style="width: 400px;" class="text-center">売却</th>
						</tr>

						<tr>
							<th class="text-center"><input type="number" name="stack"
								class="form-control" required></th>
							<th class="text-center">
								<div class="custom-control custom-radio">
									<input type="radio" name="trade" id="customRadio1" value=""
										class="custom-control-input" checked> <label
										class="custom-control-label" for="customRadio1">可</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" name="trade" id="customRadio2" value="1"
										class="custom-control-input"> <label
										class="custom-control-label" for="customRadio2">不可</label>
								</div>
							</th>
							<th class="text-center">
								<div class="custom-control custom-radio">
									<input type="radio" name="sale" id="customRadio3" value=""
										class="custom-control-input" checked> <label
										class="custom-control-label" for="customRadio3">可</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" name="sale" id="customRadio4" value="1"
										class="custom-control-input"> <label
										class="custom-control-label" for="customRadio4">不可</label>
								</div>
							</th>
						</tr>

					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<th style="width: 1200px;" class="text-center">備考</th>
						</tr>

						<tr>
							<th class="text-center"><input name="remarks" type="text"
								class="form-control"></th>
						</tr>

					</table>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-auto mx-auto">
					<button type="submit" style="width: 200px;"
						class="btn btn-secondary">登録</button>
				</div>
			</div>
		</form>
		<br>
		<div class="row">
			<div class="mx-auto">
				<a class="link text-info" href="EditMenuServlet">編集メニューに戻る</a>
			</div>
		</div>
	</div>
</body>
</html>
