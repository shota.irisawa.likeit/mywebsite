﻿USE myWebsite_db;

SELECT
	item_data.name
FROM
	item_data
WHERE
	item_data.name LIKE '%%' || item_data.detail LIKE '%%';
	
SELECT
	mob_data.name
FROM
	mob_data
WHERE
	mob_data.name LIKE '%%' || mob_data.map LIKE '%%';
	
SELECT
	boss_data.name
FROM
	boss_data
WHERE
	boss_data.name LIKE '%%' || boss_data.map LIKE '%%';
