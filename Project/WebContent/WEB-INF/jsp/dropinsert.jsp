<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Drop Insert</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"></li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link text-danger"
				 href="LogoutServlet">ログアウト</a></li>
		</ul>
	</nav>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-auto mx-auto">
				<h2>
					<strong>Drop Insert</strong>
				</h2>
			</div>
		</div>
		<br>
		<form action="DropInsertServlet" method="post">
			<div class="row">
				<div class="col-auto mx-auto">
					<div class="col-auto mx-auto text-success">${msg}</div>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mx-auto">
					<div class="col-auto mx-auto text-danger">${msg2}</div>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mx-auto">
					<div class="col-auto mx-auto text-danger">${msg3}</div>
				</div>
			</div>

			<div class="row">
				<div class="ml-auto"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-auto">
					<input type="text" name="isearchWord" style="width: 400px;"
						class="form-control" value="${isearchWord}">
				</div>
				<div class="col-auto">
					<button type="submit" style="width: 200px;"
						class="btn btn-secondary">アイテム検索</button>
				</div>
			</div>
			<div class="row">

				<div class="col-auto mx-auto"
					style="width: auto; height: 20vh; overflow-y: scroll;">

					<table class="table-bordered">

						<c:forEach var="item" items="${itemList}">
							<tr>
								<td style="width: 1200px; word-wrap: break-word;"
									class="text-center"><input type="checkbox"
									id="item${item.id }" name="itemid" value="${item.id}"
									class="form-check-input"> <label
									class="form-check-label" for="item${item.id }"></label> <a
									class="link text-info" href="ItemdetailsServlet?id=${item.id}">
										${item.name} ${item.remarks} </a></td>

							</tr>
						</c:forEach>

					</table>
				</div>

			</div>
			<br>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-auto">
					<input type="text" name="msearchWord" style="width: 400px;"
						class="form-control" value="${msearchWord}">
				</div>
				<div class="col-auto">
					<button type="submit" style="width: 200px;"
						class="btn btn-secondary">モンスター検索</button>
				</div>
			</div>
			<div class="row">

				<div class="col-auto mx-auto">
					<input type="radio" id="1" name="searchOption" value="0"
						class="custom-control-input" ${searchB}> <label
						class="custom-control-label" for="1">ボスを検索</label>

				</div>

				&emsp;

				<div class="col-auto mx-auto">
					<input type="radio" id="2" name="searchOption" value="1"
						class="custom-control-input" ${searchM}> <label
						class="custom-control-label" for="2">モブを検索</label>

				</div>
			</div>
			<div class="row">

				<div class="col-auto mx-auto"
					style="width: auto; height: 20vh; overflow-y: scroll;">

					<table class="table-bordered">

						<c:forEach var="boss" items="${bossList}">
							<tr>
								<td style="width: 1200px; word-wrap: break-word;"
									class="text-center"><input type="radio"
									id="boss${boss.id }" name="bossid" value="${boss.id}"
									class="custom-control-input"> <label
									class="custom-control-label" for="boss${boss.id }"></label> <a
									class="link text-info" href="BossdetailsServlet?id=${boss.id}">【ボス】${boss.name}
										${boss.remarks}</a></td>
							</tr>
						</c:forEach>
						<c:forEach var="mob" items="${mobList}">
							<tr>
								<td style="width: 1200px; word-wrap: break-word;"
									class="text-center"><input type="radio" id="mob${mob.id }"
									name="mobid" value="${mob.id}" class="custom-control-input">
									<label class="custom-control-label" for="mob${mob.id }"></label>
									<a class="link text-info" href="MobdetailsServlet?id=${mob.id}">【モブ】${mob.name}
										${mob.remarks}</a></td>
							</tr>
						</c:forEach>
					</table>
				</div>

			</div>
			<br>

			<div class="row">
				<div class="col-auto mx-auto">
					<button type="submit" style="width: 200px;"
						class="btn btn-secondary">登録</button>
				</div>
			</div>

		</form>
		<br>
		<div class="row">
			<div class="mx-auto">
				<a class="link text-info" href="EditMenuServlet">編集メニューに戻る</a>
			</div>
		</div>

	</div>

</body>
</html>
