package model;

import java.io.Serializable;

public class BossBeans implements Serializable {

	private int id;
	private String name;
	private String map;
	private int lv;
	private int hp;
	private int exp;
	private int difficulty;
	private int bbreak;
	private String element;
	private String remarks;

	public BossBeans (int id, String name) {
		this.id = id;
		this.name = name;
	}

	public BossBeans (int id, String name, String map, String remarks) {
		this.id = id;
		this.name = name;
		this.map = map;
		this.remarks = remarks;
	}

	public BossBeans (int id, String name, String map, int lv, int hp, int exp, int difficulty, int bbreak, String element, String remarks) {
		this.id = id;
		this.name = name;
		this.map = map;
		this.lv = lv;
		this.hp = hp;
		this.exp = exp;
		this.difficulty = difficulty;
		this.bbreak = bbreak;
		this.element = element;
		this.remarks = remarks;

	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMap() {
		return map;
	}
	public void setMap(String map) {
		this.map = map;
	}
	public int getLv() {
		return lv;
	}
	public void setLv(int lv) {
		this.lv = lv;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getExp() {
		return exp;
	}
	public void setExp(int exp) {
		this.exp = exp;
	}
	public int getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}
	public int getbbreak() {
		return bbreak;
	}
	public void setbbreak(int bbreak) {
		this.bbreak = bbreak;
	}
	public String getElement() {
		return element;
	}
	public void setElement(String element) {
		this.element = element;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
