package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ItemDao;
import model.BossBeans;
import model.ItemBeans;
import model.MobBeans;

/**
 * Servlet implementation class ItemdetailsServlet
 */
@WebServlet("/ItemdetailsServlet")
public class ItemdetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemdetailsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		if(request.getParameter("id") == null) {

			request.setAttribute("data", 0);
			request.setAttribute("iSearch", "checked");
			request.setAttribute("bSearch", "checked");
			request.setAttribute("mSearch", "checked");
			request.setAttribute("searchA", "checked");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);

		} else {

			try {

				String Sid = request.getParameter("id");
				int id = Integer.parseInt(Sid);

				//IDをもとにアイテム情報を取得
				ItemDao itemDao = new ItemDao();
				List<ItemBeans> item = itemDao.searchId(id);
				List<MobBeans> mob = itemDao.itemmobsearchId(id);
				List<BossBeans> boss = itemDao.itembosssearchId(id);

				if(item == null) {

					request.setAttribute("data", 0);
					request.setAttribute("iSearch", "checked");
					request.setAttribute("bSearch", "checked");
					request.setAttribute("mSearch", "checked");
					request.setAttribute("searchA", "checked");
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
					dispatcher.forward(request, response);

				} else {

					//アイテム情報とドロップ情報をセットし「Item」へフォワード
					request.setAttribute("item", item);
					request.setAttribute("mob", mob);
					request.setAttribute("boss", boss);

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemdetails.jsp");
					dispatcher.forward(request, response);

				}

			} catch (Exception e) {

				request.setAttribute("data", 0);
				request.setAttribute("iSearch", "checked");
				request.setAttribute("bSearch", "checked");
				request.setAttribute("mSearch", "checked");
				request.setAttribute("searchA", "checked");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
				dispatcher.forward(request, response);

			}

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
