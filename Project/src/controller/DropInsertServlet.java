package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BossDao;
import dao.ItemBossDao;
import dao.ItemDao;
import dao.ItemMobDao;
import dao.MobDao;
import model.BossBeans;
import model.ItemBeans;
import model.MobBeans;

/**
 * Servlet implementation class DropInsertServlet
 */
@WebServlet("/DropInsertServlet")
public class DropInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DropInsertServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {

			//データをセットし「Drop Insert」へフォワード
			ItemDao ItemDao = new ItemDao();
			BossDao BossDao = new BossDao();

			List<ItemBeans> itemList;
			itemList = ItemDao.all();
			request.setAttribute("itemList", itemList);

			List<BossBeans> bossList;
			bossList = BossDao.all();
			request.setAttribute("bossList", bossList);

			request.setAttribute("searchB", "checked");
			request.setAttribute("msg", "");
			request.setAttribute("textcolor", "");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/dropinsert.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {
			//文字化け防止
			request.setCharacterEncoding("UTF-8");

			//フォームの入力情報を取得し、データを新規登録。ただし既に登録されているデータとは重複させない

			int success = 0;
			int failure = 0;
			int error = 0;

			String searchOption = request.getParameter("searchOption");

			if(request.getParameterValues("itemid") != null) {

				String[] iId = request.getParameterValues("itemid");

				if(request.getParameter("bossid") != null) {

					for(int i = 1; i <= iId.length; i++) {
						String bossid = request.getParameter("bossid");
						int boss_id = Integer.parseInt(bossid);

						String itemid = iId[iId.length - i];
						int item_id = Integer.parseInt(itemid);

						dao.ItemBossDao ItemBossDao = new ItemBossDao();

						int m = ItemBossDao.bossDropInsert(boss_id, item_id);
						switch (m) {
						case 0:
							success = success + 1;
							break;
						case 1:
							failure = failure + 1;
							break;
						case 2:
							error = error + 1;
							break;
						}
					}
				}

				if(request.getParameter("mobid") != null) {

					for(int i = 1; i <= iId.length; i++) {
						String mobid = request.getParameter("mobid");
						int mob_id = Integer.parseInt(mobid);

						String itemid = iId[iId.length - i];
						int item_id = Integer.parseInt(itemid);

						dao.ItemMobDao ItemMobDao = new ItemMobDao();

						int m = ItemMobDao.mobDropInsert(mob_id, item_id);
						switch (m) {
						case 0:
							success = success + 1;
							break;
						case 1:
							failure = failure + 1;
							break;
						case 2:
							error = error + 1;
							break;
						}
					}
				}

			}

			ItemDao ItemDao = new ItemDao();
			BossDao BossDao = new BossDao();
			MobDao MobDao = new MobDao();

			List<ItemBeans> itemList;


			if(request.getParameter("isearchWord").length() != 0) {
				String isw = request.getParameter("isearchWord");
				String[] isearchWord = isw.split("[　 ]");
				itemList = ItemDao.searchE(isearchWord, "OR");
				request.setAttribute("isearchWord", isw);
			} else {
				itemList = ItemDao.all();
			}
			request.setAttribute("itemList", itemList);

			List<MobBeans> mobList;
			List<BossBeans> bossList;

			if(searchOption.equals("0")) {
				if(request.getParameter("msearchWord").length() != 0) {
					String msw = request.getParameter("msearchWord");
					String[] msearchWord = msw.split("[　 ]");
					bossList = BossDao.search(msearchWord, "AND");
					request.setAttribute("msearchWord", msw);
				} else {
					bossList = BossDao.all();
				}
				request.setAttribute("searchB", "checked");
				request.setAttribute("bossList", bossList);
			} else {

			}

			//メッセージをセットし「Drop Insert」へフォワード
			if(searchOption.equals("1")) {
				if(request.getParameter("msearchWord").length() != 0) {
					String msw = request.getParameter("msearchWord");
					String[] msearchWord = msw.split("[　 ]");
					mobList = MobDao.search(msearchWord, "AND");
					request.setAttribute("msearchWord", msw);
				} else {
					mobList = MobDao.all();
				}
				request.setAttribute("searchM", "checked");
				request.setAttribute("mobList", mobList);
			} else {

			}

			if(success != 0) {
				request.setAttribute("msg", "ドロップデータを" + success + "件登録しました");
			}

			if(failure != 0) {
				request.setAttribute("msg2", failure + "件のデータは既に登録済みです");
			}

			if(error != 0) {
				request.setAttribute("msg3", error + "件のデータでエラーが発生しました");
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/dropinsert.jsp");
			dispatcher.forward(request, response);


		}
	}
}
