package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BossDao;
import dao.ItemBossDao;
import model.BossBeans;
import model.ItemBeans;

/**
 * Servlet implementation class BossDeleteServlet
 */
@WebServlet("/BossDeleteServlet")
public class BossDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BossDeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {

			if(request.getParameter("id") == null) {

				request.setAttribute("data", 0);
				request.setAttribute("iSearch", "checked");
				request.setAttribute("bSearch", "checked");
				request.setAttribute("mSearch", "checked");
				request.setAttribute("searchA", "checked");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
				dispatcher.forward(request, response);

			} else {

				try {

					String Sid = request.getParameter("id");
					int id = Integer.parseInt(Sid);

					//IDをもとにボス情報を取得
					BossDao BossDao = new BossDao();
					List<BossBeans> boss = BossDao.searchId(id);
					List<ItemBeans> item = BossDao.bossitemsearchId(id);

					if(boss == null) {

						request.setAttribute("data", 0);
						request.setAttribute("iSearch", "checked");
						request.setAttribute("bSearch", "checked");
						request.setAttribute("mSearch", "checked");
						request.setAttribute("searchA", "checked");
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
						dispatcher.forward(request, response);

					} else {

						//ボス情報をセットし「Delete」へフォワード
						request.setAttribute("boss", boss);
						request.setAttribute("item", item);

						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bossdelete.jsp");
						dispatcher.forward(request, response);

					}

				} catch (Exception e) {

					request.setAttribute("data", 0);
					request.setAttribute("iSearch", "checked");
					request.setAttribute("bSearch", "checked");
					request.setAttribute("mSearch", "checked");
					request.setAttribute("searchA", "checked");
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
					dispatcher.forward(request, response);

				}
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {

			//文字化け防止
			request.setCharacterEncoding("UTF-8");

			//フォームの入力情報を取得、条件に一致する場合のみデータを削除。関連するドロップデータも全て削除
			String id0 = request.getParameter("id");
			String dword = request.getParameter("deleteword");

			if(dword.equals("削除します")) {

				int id = 0;

				try {
					id = Integer.parseInt(id0);
				} catch (Exception e) {

				}

				int msgA = 0;
				int msgB = 0;
				int drop = 0;

				if(request.getParameterValues("itemid") != null) {

					String[] iId = request.getParameterValues("itemid");
					for(int i = 1; i <= iId.length; i++) {

						String itemId = iId[iId.length - i];
						int item_id = Integer.parseInt(itemId);

						dao.ItemBossDao ItemBossDao = new ItemBossDao();

						msgA = msgA + ItemBossDao.bossDropDelete(id, item_id);
						drop = drop + 1;
					}
				}

				BossDao BossDao = new BossDao();

				msgB = msgB + BossDao.BossDelete(id);

				//メッセージをセットし「Edit Menu」へフォワード
				String msg;
				if(msgA * msgB == 0) {
					if(drop == 0) {
						msg = "ボスデータを1件削除しました";
					} else {
						msg =  "ボスデータを1件、ドロップデータを" + drop + "件削除しました";
					}
					request.setAttribute("msg", msg);
				} else {
					request.setAttribute("msg2", "エラーが発生しました");
				}

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editmenu.jsp");
				dispatcher.forward(request, response);
			} else {
				request.setAttribute("msg2", "削除の実行に失敗しました");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editmenu.jsp");
				dispatcher.forward(request, response);

			}

		}
	}
}


