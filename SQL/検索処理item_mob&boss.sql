USE myWebsite_db;

SELECT
	mob_data.name, mob_data.map, mob_data.lv
FROM
	item_data
JOIN 
	item_and_mob
ON
	item_data.id = item_and_mob.item_id
JOIN
	mob_data
ON
	mob_data.id = item_and_mob.mob_id
WHERE
	item_data.id = 228;

SELECT
	boss_data.name, boss_data.map, boss_data.lv
FROM
	item_data
JOIN 
	item_and_boss
ON
	item_data.id = item_and_boss.item_id
JOIN
	boss_data
ON
	boss_data.id = item_and_boss.boss_id
WHERE
	item_data.id = 228;
