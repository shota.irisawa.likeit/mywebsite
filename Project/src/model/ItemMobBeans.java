package model;

import java.io.Serializable;

public class ItemMobBeans implements Serializable {

	private int id;
	private int mob_id;
	private int item_id;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMob_id() {
		return mob_id;
	}
	public void setMob_id(int mob_id) {
		this.mob_id = mob_id;
	}
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}


}
