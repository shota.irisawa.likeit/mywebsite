package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemMobDao;
import dao.MobDao;
import model.ItemBeans;
import model.MobBeans;

/**
 * Servlet implementation class MobUpdateServlet
 */
@WebServlet("/MobUpdateServlet")
public class MobUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MobUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {
			if(request.getParameter("id") == null) {

				request.setAttribute("data", 0);
				request.setAttribute("iSearch", "checked");
				request.setAttribute("bSearch", "checked");
				request.setAttribute("mSearch", "checked");
				request.setAttribute("searchA", "checked");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
				dispatcher.forward(request, response);

			} else {

				try {

					String Sid = request.getParameter("id");
					int id = Integer.parseInt(Sid);

					//IDをもとにモブ情報を取得
					MobDao MobDao = new MobDao();
					List<MobBeans> mob = MobDao.searchId(id);
					List<ItemBeans> item = MobDao.mobitemsearchId(id);

					if(mob == null) {

						request.setAttribute("data", 0);
						request.setAttribute("iSearch", "checked");
						request.setAttribute("bSearch", "checked");
						request.setAttribute("mSearch", "checked");
						request.setAttribute("searchA", "checked");
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
						dispatcher.forward(request, response);

					} else {

						//モブ情報をセットし「Monster Update」へフォワード
						request.setAttribute("mob", mob);
						request.setAttribute("item", item);

						switch(mob.get(0).getElement()) {
						case "無属性":
							request.setAttribute("e1", "selected");
							break;
						case "火属性":
							request.setAttribute("e2", "selected");
							break;
						case "地属性":
							request.setAttribute("e3", "selected");
							break;
						case "風属性":
							request.setAttribute("e4", "selected");
							break;
						case "水属性":
							request.setAttribute("e5", "selected");
							break;
						case "光属性":
							request.setAttribute("e6", "selected");
							break;
						case "闇属性":
							request.setAttribute("e7", "selected");
							break;
						}

						switch(mob.get(0).getCategory()) {
						case "モブ":
							request.setAttribute("c0", "checked");
							break;
						case "フィールドボス":
							request.setAttribute("c1", "checked");
							break;
						}

						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/mobupdate.jsp");
						dispatcher.forward(request, response);

					}

				} catch(Exception e) {

					request.setAttribute("data", 0);
					request.setAttribute("iSearch", "checked");
					request.setAttribute("bSearch", "checked");
					request.setAttribute("mSearch", "checked");
					request.setAttribute("searchA", "checked");
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editindex.jsp");
					dispatcher.forward(request, response);

				}
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {
			//文字化け防止
			request.setCharacterEncoding("UTF-8");

			//フォームの入力情報を取得、データを更新。選択されたドロップデータを削除
			String name = request.getParameter("name");
			String map = request.getParameter("map");
			String category = request.getParameter("category");
			String element = request.getParameter("element");
			String lv = request.getParameter("lv");
			String exp = request.getParameter("exp");
			String remarks = request.getParameter("remarks");
			String id = request.getParameter("id");

			int msgA = 0;
			int msgB = 0;
			int drop = 0;

			if(request.getParameterValues("itemid") != null) {

				String[] iId = request.getParameterValues("itemid");
				for(int i = 1; i <= iId.length; i++) {
					String mobId = request.getParameter("id");
					int mob_id = Integer.parseInt(mobId);

					String itemId = iId[iId.length - i];
					int item_id = Integer.parseInt(itemId);

					dao.ItemMobDao ItemMobDao = new ItemMobDao();

					msgA = msgA + ItemMobDao.mobDropDelete(mob_id, item_id);
					drop = drop + 1;
				}
			}

			MobDao MobDao = new MobDao();

			msgB = msgB + MobDao.MobUpdate(name, map, category, element, lv, exp, remarks, id);

			String msg;
			if(msgA * msgB == 0) {
				if(drop == 0) {
					msg = "モンスターデータを1件更新しました";
				} else {
					msg =  "モンスターデータを1件更新、ドロップデータを" + drop + "件削除しました";
				}
				request.setAttribute("msg", msg);
			} else {
				request.setAttribute("msg2", "エラーが発生しました");
			}


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editmenu.jsp");
			dispatcher.forward(request, response);
		}
	}

}
