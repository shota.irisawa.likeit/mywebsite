package model;

import java.io.Serializable;

public class ItemBossBeans implements Serializable {

	private int id;
	private int boss_id;
	private int item_id;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBoss_id() {
		return boss_id;
	}
	public void setBoss_id(int boss_id) {
		this.boss_id = boss_id;
	}
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}

}
