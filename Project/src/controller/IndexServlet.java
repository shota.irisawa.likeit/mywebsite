package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BossDao;
import dao.ItemDao;
import dao.MobDao;
import model.BossBeans;
import model.ItemBeans;
import model.MobBeans;

/**
 * Servlet implementation class index
 */
@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setAttribute("data", 0);
		request.setAttribute("iSearch", "checked");
		request.setAttribute("bSearch", "checked");
		request.setAttribute("mSearch", "checked");
		request.setAttribute("searchA", "checked");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//フォームの入力情報を取得
		String searchWord = request.getParameter("searchWord");
		String itemSearch = request.getParameter("itemSearch");
		String bossSearch = request.getParameter("bossSearch");
		String mobSearch = request.getParameter("mobSearch");
		String searchOption = request.getParameter("searchoption");

		//入力情報をスペースで分割
		String[] searchWords = searchWord.split("[　 ]");

		//入力された情報に該当するデータを検索、取得、セットし「Toram Online Database」へフォワード
		ItemDao itemDao = new ItemDao();
		int data = 0;

		List<ItemBeans> itemList;
		if(itemSearch != null) {
			itemList = itemDao.search(searchWords, searchOption);
			request.setAttribute("itemList", itemList);
			request.setAttribute("iSearch", "checked");
			data = data + itemList.size();
;		} else {

		}

		BossDao bossDao = new BossDao();
		List<BossBeans> bossList;
		if(bossSearch != null) {
			bossList = bossDao.search(searchWords, searchOption);
			request.setAttribute("bossList", bossList);
			request.setAttribute("bSearch", "checked");
			data = data + bossList.size();

		} else {

		}

		MobDao mobDao = new MobDao();
		List<MobBeans> mobList;

		if(mobSearch != null) {
			mobList = mobDao.search(searchWords, searchOption);
			request.setAttribute("mobList", mobList);
			request.setAttribute("mSearch", "checked");
			data = data + mobList.size();

		} else {

		}

		if(searchOption.equals("AND")) {

			request.setAttribute("searchA", "checked");


		} else if(searchOption.equals("OR")) {

			request.setAttribute("searchO", "checked");

		}

		String hit;
		if(data == 0){
			hit = "該当データは見つかりませんでした";
		} else {
			hit = data + "件のデータがヒットしました";
		}

		request.setAttribute("data", data);
		request.setAttribute("hit", hit);

		request.setAttribute("searchWord", searchWord);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
		dispatcher.forward(request, response);
	}

}
