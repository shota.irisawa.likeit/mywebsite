<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:forEach var="item" items="${item}">
<title>【Item Data】 ${item.name}</title>
</c:forEach>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"></li>
		</ul>
		<ul class="navbar-nav">
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link text-danger" href="EditMenuServlet">編集</a></li>
		</ul>
	</nav>
	<br>
	<c:forEach var="item" items="${item}">
		<div class="container">
			<div class="row">
				<div class="col-auto mx-auto">
					<h2>
						<strong>Item Data</strong>
					</h2>
				</div>
			</div>
		</div>

		<br>
		<div class="row">
			<div class="col-auto mx-auto">
				<table class="table-bordered">

					<tr class="table-secondary">
						<td style="width: 1200px;" class="text-center">アイテム名</td>
					</tr>

					<tr>
						<td class="text-center">${item.name}</td>
					</tr>

				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-auto mx-auto">
				<table class="table-bordered">

					<tr class="table-secondary">
						<td style="width: 1200px;" class="text-center">カテゴリー</td>
					</tr>

					<tr>
						<td class="text-center">${item.category}</td>
					</tr>

				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-auto mx-auto">
				<c:if test="${item.atk.length() != 0}">
					<table class="table-bordered">
						<tr class="table-secondary">
							<td style="width: 600px;" class="text-center">基礎ATK</td>
							<td style="width: 600px;" class="text-center">安定率</td>
						</tr>

						<tr>
							<td class="text-center">${item.atk}</td>
							<td class="text-center">${item.stability}%</td>
						</tr>
					</table>
				</c:if>

				<c:if test="${item.def.length() != 0}">
					<table class="table-bordered">
						<tr class="table-secondary">
							<td style="width: 1200px;" class="text-center">基礎DEF</td>
						</tr>

						<tr>
							<td class="text-center">${item.def}</td>
						</tr>
					</table>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-auto mx-auto">
				<c:if test="${item.detail.length() != 0}">
					<table class="table-bordered">

						<tr class="table-secondary">
							<td style="width: 1200px;" class="text-center">プロパティ</td>
						</tr>

						<tr>
							<td class="text-center">${item.detail}</td>
						</tr>
					</table>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-auto mx-auto">
				<c:if test="${item.hidden_property.length() != 0}">
					<table class="table-bordered">

						<tr class="table-secondary">
							<td style="width: 1200px;" class="text-center">隠しプロパティ</td>
						</tr>

						<tr>
							<td class="text-center">${item.hidden_property}</td>
						</tr>

					</table>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-auto mx-auto">
				<c:if
					test="${item.colorA.length() != 0 || item.colorB.length() != 0 || item.colorC.length() != 0}">
					<table class="table-bordered">

						<tr class="table-secondary">
							<td style="width: 400px;" class="text-center">染色A</td>
							<td style="width: 400px;" class="text-center">染色B</td>
							<td style="width: 400px;" class="text-center">染色C</td>
						</tr>

						<tr>
							<td class="text-center">${item.colorA}</td>
							<td class="text-center">${item.colorB}</td>
							<td class="text-center">${item.colorC}</td>
						</tr>

					</table>
				</c:if>

			</div>
		</div>
		<div class="row">
			<div class="col-auto mx-auto">
				<table class="table-bordered">

					<tr class="table-secondary">
						<td style="width: 400px;" class="text-center">1s</td>
						<td style="width: 400px;" class="text-center">トレード</td>
						<td style="width: 400px;" class="text-center">売却</td>
					</tr>

					<tr>
						<td class="text-center">${item.stack}</td>
						<td class="text-center">${item.cannot_trade}</td>
						<td class="text-center">${item.cannot_sale}</td>
					</tr>

				</table>
			</div>
		</div>
	</c:forEach>
	<div class="row">
		<div class="col-auto mx-auto">
			<table class="table-bordered">

				<tr class="table-secondary">
					<td style="width: 1200px;" class="text-center">入手</td>
				</tr>

				<c:forEach var="mob" items="${mob}">
					<tr>
						<td class="text-center"><a class="link text-info"
							href="MobdetailsServlet?id=${mob.id}">【モンスター】 ${mob.name} ／
								${mob.map}</a></td>
					</tr>
				</c:forEach>
				<c:forEach var="boss" items="${boss}">
					<tr>
						<td class="text-center"><a class="link text-info"
							href="BossdetailsServlet?id=${boss.id}">【ボスモンスター】 ${boss.name} ／
								${boss.map}</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<a class="link text-info" href="IndexServlet">TOPへ戻る</a>
		</div>
	</div>
</body>
</html>
