<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>【Edit Index】</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"></li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link text-danger"
				href="LogoutServlet">ログアウト</a></li>
		</ul>
	</nav>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-auto mx-auto">
				<h2>
					<strong>Edit Index</strong>
				</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="ml-auto"></div>
		</div>
		<form action="EditIndexServlet" method="post">
			<div class="row">
				<div class="col-3"></div>
				<div class="col-auto">
					<input type="text" name="searchWord" style="width: 400px;"
						value="${searchWord}" class="form-control">
				</div>
				<div class="col-auto">
					<button type="submit" style="width: 200px;"
						class="btn btn-secondary">検索</button>
				</div>

			</div>
			<br>

			<div class="row">

				<div class="col-auto ml-auto">
					<input class="form-check-input" type="radio" name="searchoption"
						value="AND" ${searchA}> <label class="form-check-label">AND検索</label>
				</div>

				&emsp;

				<div class="col-auto mr-auto">
					<input class="form-check-input" type="radio" name="searchoption"
						value="OR" ${searchO}> <label class="form-check-label">OR検索</label>
				</div>
			</div>
			<br>

			<div class="row">

				<div class="col-auto mx-auto">
					<input class="form-check-input" type="checkbox" name="itemSearch"
						value="1" ${iSearch}> <label class="form-check-label">アイテムを検索結果に含める</label>
				</div>

				<div class="col-auto mx-auto">
					<input class="form-check-input" type="checkbox" name="bossSearch"
						value="1" ${bSearch}> <label class="form-check-label">ボスモンスターを検索結果に含める</label>
				</div>


				<div class="col-auto mx-auto">
					<input class="form-check-input" type="checkbox" name="mobSearch"
						value="1" ${mSearch}> <label class="form-check-label">フィールドモンスターを検索結果に含める</label>
				</div>

			</div>
		</form>

		<hr>

		<div class="row">
			<div class="col-auto mx-auto">${hit}</div>
		</div>
		<c:if test="${data != 0}">
			<div class="row">
				<div class="col-auto mx-auto"
					style="width: auto; height: 30vh; overflow-y: scroll;">

					<table class="table-bordered">

						<c:forEach var="item" items="${itemList}">
							<tr>
								<td style="width: 800px;" class="text-center">【アイテム】
									${item.name}</td>
								<td style="width: 200px;" class="text-center"><a
									class="btn btn-success" href="ItemUpdateServlet?id=${item.id}"
									role="button">編集</a></td>
								<td style="width: 200px;" class="text-center"><a
									class="btn btn-danger" href="ItemDeleteServlet?id=${item.id}"
									role="button">削除</a></td>
							</tr>
						</c:forEach>

						<c:forEach var="boss" items="${bossList}">
							<tr>
								<td style="width: 800px;" class="text-center">【ボスモンスター】
									${boss.name} ／ ${boss.map }</td>
								<td style="width: 200px;" class="text-center"><a
									class="btn btn-success" href="BossUpdateServlet?id=${boss.id}"
									role="button">編集</a></td>
								<td style="width: 200px;" class="text-center"><a
									class="btn btn-danger" href="BossDeleteServlet?id=${boss.id}"
									role="button">削除</a></td>
							</tr>
						</c:forEach>

						<c:forEach var="mob" items="${mobList}">
							<tr>
								<td style="width: 800px;" class="text-center">【モンスター】
									${mob.name} ／ ${mob.map }</td>
								<td style="width: 200px;" class="text-center"><a
									class="btn btn-success" href="MobUpdateServlet?id=${mob.id}"
									role="button">編集</a></td>
								<td style="width: 200px;" class="text-center"><a
									class="btn btn-danger" href="MobDeleteServlet?id=${mob.id}"
									role="button">削除</a></td>
							</tr>
						</c:forEach>

					</table>
				</div>

			</div>
		</c:if>
	</div>

	<br>
	<div class="row">
		<div class="mx-auto">
			<a class="link text-info" href="EditMenuServlet">編集メニューに戻る</a>
		</div>
	</div>

</body>
</html>
