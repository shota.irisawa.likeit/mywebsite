package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BossDao;
import dao.ItemBossDao;
import model.BossBeans;
import model.ItemBeans;

/**
 * Servlet implementation class BossUpdateServlet
 */
@WebServlet("/BossUpdateServlet")
public class BossUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BossUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {
			if(request.getParameter("id") == null) {

				request.setAttribute("data", 0);
				request.setAttribute("iSearch", "checked");
				request.setAttribute("bSearch", "checked");
				request.setAttribute("mSearch", "checked");
				request.setAttribute("searchA", "checked");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
				dispatcher.forward(request, response);

			} else {

				try {

					String Sid = request.getParameter("id");
					int id = Integer.parseInt(Sid);

					//IDをもとにボス情報を取得
					BossDao BossDao = new BossDao();
					List<BossBeans> boss = BossDao.searchId(id);
					List<ItemBeans> item = BossDao.bossitemsearchId(id);

					if(boss == null) {

						request.setAttribute("data", 0);
						request.setAttribute("iSearch", "checked");
						request.setAttribute("bSearch", "checked");
						request.setAttribute("mSearch", "checked");
						request.setAttribute("searchA", "checked");
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
						dispatcher.forward(request, response);

					} else {

						switch(boss.get(0).getElement()) {
						case "無属性":
							request.setAttribute("e1", "selected");
							break;
						case "火属性":
							request.setAttribute("e2", "selected");
							break;
						case "地属性":
							request.setAttribute("e3", "selected");
							break;
						case "風属性":
							request.setAttribute("e4", "selected");
							break;
						case "水属性":
							request.setAttribute("e5", "selected");
							break;
						case "光属性":
							request.setAttribute("e6", "selected");
							break;
						case "闇属性":
							request.setAttribute("e7", "selected");
							break;
						}

						switch(boss.get(0).getDifficulty()) {
						case 0:
							request.setAttribute("d0", "checked");
							break;
						case 1:
							request.setAttribute("d1", "checked");
							break;
						case 2:
							request.setAttribute("d2", "checked");
							break;
						}

						//ボス情報をセットし「Boss Update」へフォワード
						request.setAttribute("boss", boss);
						request.setAttribute("item", item);

						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bossupdate.jsp");
						dispatcher.forward(request, response);
					}

				} catch (Exception e) {

					request.setAttribute("data", 0);
					request.setAttribute("iSearch", "checked");
					request.setAttribute("bSearch", "checked");
					request.setAttribute("mSearch", "checked");
					request.setAttribute("searchA", "checked");
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editindex.jsp");
					dispatcher.forward(request, response);

				}

			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {

			//文字化け防止
			request.setCharacterEncoding("UTF-8");

			//フォームの入力情報を取得、データを更新。選択されたドロップデータを削除
			String name = request.getParameter("name");
			String map = request.getParameter("map");
			String element = request.getParameter("element");
			String difficulty = request.getParameter("difficulty");
			String blv = request.getParameter("lv");
			String bhp = request.getParameter("hp");
			String bexp = request.getParameter("exp");
			String bbbreak = request.getParameter("bbreak");
			String remarks = request.getParameter("remarks");
			String id = request.getParameter("id");

			int lv = 0;
			int hp = 0;
			int exp = 0;
			int bbreak = 0;

			String error = "";

			try {
				lv = Integer.parseInt(blv);
				hp = Integer.parseInt(bhp);
				exp = Integer.parseInt(bexp);
				bbreak = Integer.parseInt(bbbreak);
			} catch (Exception e) {
				error = "、不適切な値が入力されていたため情報の一部が正しく反映されていません";
			}

			int msgA = 0;
			int msgB = 0;
			int drop = 0;

			if(request.getParameterValues("itemid") != null) {

				String[] iId = request.getParameterValues("itemid");
				for(int i = 1; i <= iId.length; i++) {
					String bossId = request.getParameter("id");
					int boss_id = Integer.parseInt(bossId);

					String itemId = iId[iId.length - i];
					int item_id = Integer.parseInt(itemId);

					dao.ItemBossDao ItemBossDao = new ItemBossDao();

					msgA = msgA + ItemBossDao.bossDropDelete(boss_id, item_id);
					drop = drop + 1;
				}
			}

			BossDao BossDao = new BossDao();

			msgB = msgB + BossDao.BossUpdate(name, map, element, difficulty, lv, hp, exp, bbreak, remarks, id);

			String msg;

			//メッセージをセットし「Edit Menu」へフォワード
			if(msgA * msgB == 0) {
				if(drop == 0) {
					msg = "ボスデータを1件更新しました" + error;
				} else {
					msg =  "ボスデータを1件更新、ドロップデータを" + drop + "件削除しました" + error;
				}
				request.setAttribute("msg", msg);
			} else {
				request.setAttribute("msg2", "エラーが発生しました");
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editmenu.jsp");
			dispatcher.forward(request, response);
		}
	}
}
