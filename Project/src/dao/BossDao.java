package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.BossBeans;
import model.ItemBeans;

public class BossDao {

	//IDをもとにボスデータを検索
	//該当データの詳細をList型のコレクションで返す
	public List<BossBeans> searchId (int id) {

		Connection conn = null;
		List<BossBeans> boss = new ArrayList<BossBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "SELECT *"
					+ " FROM boss_data"
					+ " WHERE boss_data.id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {

				String name = rs.getString("name");
				String map = rs.getString("map");
				int lv = rs.getInt("lv");
				int exp = rs.getInt("exp");
				int hp = rs.getInt("hp");
				int difficulty = rs.getInt("difficulty");
				int bbreak = rs.getInt("break");
				String element = rs.getString("element");
				String remarks = rs.getString("remarks");

				BossBeans bossDetail = new BossBeans(id, name, map, lv, hp, exp, difficulty, bbreak, element, remarks);

				boss.add(bossDetail);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return boss;

	}

	//ボスIDをもとにドロップするアイテムのIDと名前を検索
	//該当データをList型のコレクションで返す
	public List<ItemBeans> bossitemsearchId (int id) {

		Connection conn = null;
		List<ItemBeans> item = new ArrayList<ItemBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "SELECT item_data.id, item_data.name, item_data.category"
					+ " FROM item_data"
					+ " JOIN item_and_boss"
					+ " ON item_data.id = item_and_boss.item_id"
					+ " JOIN boss_data"
					+ " ON item_and_boss.boss_id = boss_data.id"
					+ " WHERE boss_data.id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int itemId = rs.getInt("id");
				String itemName = rs.getString("name");
				String itemCategory = rs.getString("category");

				ItemBeans itemDetail = new ItemBeans(itemId, itemName, itemCategory, "");

				item.add(itemDetail);

			}


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return item;

	}

	//検索ワードをもとにボスデータを検索
	//該当データをすべてList型のコレクションで返す
	public List<BossBeans> search (String[] searchWords, String searchOption) {

		Connection conn = null;
		List<BossBeans> bossList = new ArrayList<BossBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;
			String word = "WHERE (boss_data.name LIKE CONCAT('%', ?, '%')"
					+ " OR boss_data.map LIKE CONCAT('%', ?, '%')"
					+ " OR boss_data.element LIKE CONCAT('%', ?, '%')"
					+ " OR boss_data.remarks LIKE CONCAT('%', ?, '%'))";

			for(int i = 2; i <= searchWords.length; i = i + 1) {
				word = word + " " + searchOption + " (boss_data.name LIKE CONCAT('%', ?, '%')"
						+ " OR boss_data.map LIKE CONCAT('%', ?, '%')"
						+ " OR boss_data.element LIKE CONCAT('%', ?, '%')"
						+ " OR boss_data.remarks LIKE CONCAT('%', ?, '%'))";
			}

			sql = "SELECT boss_data.name, boss_data.id, boss_data.map"
					+ " FROM boss_data"
					+ " " + word
					+ " ORDER BY boss_data.name";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			for(int j = 1; j <= searchWords.length; j = j + 1) {

				pStmt.setString((4 * j) - 3, searchWords[j - 1]);
				pStmt.setString((4 * j) - 2, searchWords[j - 1]);
				pStmt.setString((4 * j) - 1, searchWords[j - 1]);
				pStmt.setString((4 * j), searchWords[j - 1]);

			}

			ResultSet rs = pStmt.executeQuery();

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String map = rs.getString("map");
				BossBeans Boss = new BossBeans(id, name, map, "");

				bossList.add(Boss);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return bossList;

	}

	//ボスデータの新規登録
	public int BossInsert (String name, String map, String element, String difficulty, int lv, int hp
			, int exp, int bbreak, String remarks) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "INSERT INTO boss_data (name, map, Lv, HP, EXP, difficulty, break, element, remarks)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, map);
			pStmt.setInt(3, lv);
			pStmt.setInt(4, hp);
			pStmt.setInt(5, exp);
			pStmt.setString(6, difficulty);
			pStmt.setInt(7, bbreak);
			pStmt.setString(8, element);
			pStmt.setString(9, remarks);

			pStmt.executeUpdate();

			return 0;

		} catch (SQLException e) {
			e.printStackTrace();
			return 1;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	//最新30件のボスデータをすべてList型のコレクションで返す
	public List<BossBeans> all () {

		Connection conn = null;
		List<BossBeans> bossList = new ArrayList<BossBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql = "SELECT boss_data.id, boss_data.name, boss_data.remarks"
					+ " FROM boss_data"
					+ " ORDER BY boss_data.id DESC"
					+ " LIMIT 30";

			Statement Stmt = conn.createStatement();

			ResultSet rs = Stmt.executeQuery(sql);

			//得られたデータをコレクションに代入
			while(rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String remarks = rs.getString("remarks");
				if(remarks.length() != 0) {
					remarks = "<br>[" + remarks + "]";
				}
				BossBeans Boss = new BossBeans(id, name, "", remarks);

				bossList.add(Boss);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}

		}
		//コレクションを戻り値として返す
		return bossList;

	}

	//ボスデータの更新
	public int BossUpdate (String name, String map, String element, String difficulty, int lv, int hp
			, int exp, int bbreak, String remarks, String id) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "UPDATE boss_data"
					+ " SET name = ?,"
					+ " map = ?,"
					+ " Lv = ?,"
					+ " HP = ?,"
					+ " EXP = ?,"
					+ " difficulty = ?,"
					+ " break = ?,"
					+ " element = ?,"
					+ " remarks = ?"
					+ " WHERE"
					+ " boss_data.id = ?"
					+ " LIMIT 1";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, map);
			pStmt.setInt(3, lv);
			pStmt.setInt(4, hp);
			pStmt.setInt(5, exp);
			pStmt.setString(6, difficulty);
			pStmt.setInt(7, bbreak);
			pStmt.setString(8, element);
			pStmt.setString(9, remarks);
			pStmt.setString(10, id);

			pStmt.executeUpdate();

			return 0;

		} catch (SQLException e) {
			e.printStackTrace();
			return 1;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	//ボスデータの削除
	public int BossDelete (int id) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;

			sql = "DELETE FROM boss_data"
					+ " WHERE id = ?"
					+ " LIMIT 1";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			pStmt.executeUpdate();

			return 0;

		} catch (SQLException e) {
			e.printStackTrace();
			return 1;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

}
