package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AdminDao;
import model.AdminBeans;
import model.Epass;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//エラーメッセージを初期化し「Login」へフォワード
		request.setAttribute("msg", null);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		//フォームの入力情報を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		//入力されたパスワードを暗号化
		Epass epass = new Epass();
		String pass = epass.encryption(password);

		//入力情報をデータベースと照合
		AdminDao AdminDao = new AdminDao();
		AdminBeans admin = AdminDao.findByLoginInfo(loginId, pass);

		//該当データが存在する場合ログイン処理を行う
		//存在しない場合エラーメッセージをセットし、「Login」へフォワード
		if(admin == null) {
			request.setAttribute("msg", "ログインに失敗しました");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		} else {

			//ログインセッションに管理者データをセットし「Edit Menu」へフォワード
			HttpSession session = request.getSession();
			session.setAttribute("AdminInfo", admin);
			request.setAttribute("msg", "ログインに成功しました");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editmenu.jsp");
			dispatcher.forward(request, response);

		}
	}

}
