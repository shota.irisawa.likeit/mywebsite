<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>【Editor Update】</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"></li>
		</ul>
		<ul class="navbar-nav">
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link text-danger" href="LogoutServlet">ログアウト</a></li>
		</ul>
	</nav>
	<br>
	<div class="container">
		<br>
		<div class="row">
			<div class="col-auto mx-auto">
				<h2>
					<strong>Editor Update</strong>
				</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-auto mx-auto text-danger">${msg}</div>
		</div>

		<br>

		<form class="form-signin" action="EditorUpdateServlet" method="post" autocomplete="off">
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-2">現在のログインID</div>
				<div class="col-sm-6">
				<input type="hidden" name="loginId" value="${AdminInfo.loginId }">
					${AdminInfo.loginId }
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-2">現在のパスワード</div>
				<div class="col-sm-6">
					<input type="password" name="password" style="width: 200px;"
						class="form-control">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-2">新しいログインID</div>
				<div class="col-sm-6">
					<input type="text" name="nloginId" style="width: 200px;"
						class="form-control" required>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-2">新しいパスワード</div>
				<div class="col-sm-6">
					<input type="password" name="npassword" style="width: 200px;"
						class="form-control" required>
				</div>
			</div>
			<br> <br>
			<div class="row">
				<div class="col-auto mx-auto">
					<button class="btn btn-secondary btn-signin" style="width: 200px;"
						type="submit">更新</button>
				</div>
			</div>
		</form>
	</div>
	<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<a class="link text-info" href="EditMenuServlet">編集メニューへ戻る</a>
		</div>
	</div>
</body>
</html>