<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>【Delete】</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"></li>
		</ul>
		<ul class="navbar-nav">
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link text-danger"
				href="LogoutServlet">ログアウト</a></li>
		</ul>
	</nav>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-auto mx-auto">
				<h2>
					<strong>Delete</strong>
				</h2>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<div class="col-auto mx-auto text-danger">以下のデータを削除します。</div>
		</div>
	</div>
	<br>
	<form action="MobDeleteServlet" method="post" autocomplete="off">
		<c:forEach var="mob" items="${mob}">
		<input type="hidden" value="${mob.id}" name="id">
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<td style="width: 1200px;" class="text-center">モンスター名</td>
						</tr>

						<tr>
							<td class="text-center">${mob.name}</td>
						</tr>

					</table>
				</div>
			</div>

			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<td style="width: 1200px;" class="text-center">出現マップ</td>
						</tr>

						<tr>
							<td class="text-center">${mob.map}</td>
						</tr>

					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<td style="width: 1200px;" class="text-center">分類</td>
						</tr>

						<tr>
							<td class="text-center">${mob.category}</td>
						</tr>

					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<td style="width: 1200px;" class="text-center">属性</td>
						</tr>

						<tr>
							<td class="text-center">${mob.element}</td>
						</tr>

					</table>
				</div>
			</div>

			<div class="row">
				<div class="col-auto mx-auto">
					<table class="table-bordered">

						<tr class="table-secondary">
							<td style="width: 600px;" class="text-center">Lv</td>
							<td style="width: 600px;" class="text-center">基礎経験値</td>

						</tr>
						<c:forEach var="mobLvExp" items="${mobLvExp}">
							<tr>
								<td class="text-center">${mobLvExp.lv}</td>

								<td class="text-center">${mobLvExp.exp}</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>

		</c:forEach>

		<div class="row">
			<div class="col-auto mx-auto">
				<table class="table-bordered">

					<tr class="table-secondary">
						<td style="width: 1200px;" class="text-center">ドロップアイテム</td>
					</tr>

					<c:forEach var="item" items="${item}">
						<tr>
							<td class="text-center"><input type="hidden"
								value="${item.id}" name="itemid">${item.category}
									${item.name}</td>
						</tr>
					</c:forEach>

				</table>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-auto mx-auto">
				<div class="col-auto mx-auto text-danger">「削除します」と入力してください</div>
			</div>
		</div>
		<div class="row">
			<div class="col-auto mx-auto">
				<input type="text" name="deleteword" style="width: 400px;"
					class="form-control">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-auto mx-auto">
				<button type="submit" style="width: 200px;"
					class="btn btn-secondary">実行</button>
			</div>
		</div>
	</form>
	<br>
	<div class="row">
		<div class="mx-auto">
			<a class="link text-info" href="EditMenuServlet">編集メニューに戻る</a>
		</div>
	</div>
</body>
</html>
