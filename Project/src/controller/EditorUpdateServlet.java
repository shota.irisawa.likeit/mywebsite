package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AdminDao;
import model.AdminBeans;
import model.Epass;

/**
 * Servlet implementation class EditorUpdateServlet
 */
@WebServlet("/EditorUpdateServlet")
public class EditorUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditorUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
    	HttpSession session = request.getSession();

    	if(session.getAttribute("AdminInfo") == null) {

    		response.sendRedirect("LoginServlet");

    	} else {
    		//エラーメッセージを初期化し「Editor Update」へフォワード
    		request.setAttribute("msg", null);
    		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editorupdate.jsp");
    		dispatcher.forward(request, response);
    	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログインセッションが存在しない場合「Login」へリダイレクト
		HttpSession session = request.getSession();

		if(session.getAttribute("AdminInfo") == null) {

			response.sendRedirect("LoginServlet");

		} else {
			request.setCharacterEncoding("UTF-8");

			//フォームの入力情報を取得
			String loginId = request.getParameter("loginId");
			String password = request.getParameter("password");
			String nloginId = request.getParameter("nloginId");
			String npassword = request.getParameter("npassword");

			//入力されたパスワードを暗号化
			Epass epass = new Epass();
			String pass = epass.encryption(password);
			String npass = epass.encryption(npassword);

			//入力情報をデータベースと照合
			AdminDao adminDao = new AdminDao();

			//一致する場合ログイン処理を行う
			//一致しない場合エラーメッセージをセットし、「Editor Update」へフォワード
			if(adminDao.findByLoginInfo(loginId, pass) == null) {
				request.setAttribute("msg", "パスワードが間違っています");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editorupdate.jsp");
				dispatcher.forward(request, response);
				return;

			} else {

				adminDao.AdminUpdate(loginId, nloginId, npass);

				AdminBeans nadmin = adminDao.findByLoginInfo(nloginId, npass);

				//ログインセッションに管理者データをセットし直し、「Edit Menu」へフォワード
				session.removeAttribute("AdminInfo");
				session.setAttribute("AdminInfo", nadmin);
				request.setAttribute("msg", "編集者情報を更新しました");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editmenu.jsp");
				dispatcher.forward(request, response);

			}

		}
	}

}
