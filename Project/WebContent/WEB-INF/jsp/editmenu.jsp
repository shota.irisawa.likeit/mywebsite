<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<title>【Edit Menu】</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"></li>
		</ul>
		<ul class="navbar-nav">
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link text-danger" href="LogoutServlet">ログアウト</a></li>
		</ul>
	</nav>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-auto mx-auto">
				<h2>
					<strong>Edit Menu</strong>
				</h2>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<div class="col-auto mx-auto text-success">${msg}</div>
		</div>
	</div>
	<div class="row">
		<div class="col-auto mx-auto">
			<div class="col-auto mx-auto text-danger">${msg2}</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<a class="link text-info" href="ItemInsertServlet">アイテム新規登録</a>
		</div>
	</div>
		<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<a class="link text-info" href="BossInsertServlet">ボス新規登録</a>
		</div>
	</div>
		<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<a class="link text-info" href="MobInsertServlet">モブ新規登録</a>
		</div>
	</div>
		<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<a class="link text-info" href="EditIndexServlet">データ更新・削除</a>
		</div>
	</div>
		<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<a class="link text-info" href="DropInsertServlet">ドロップデータ登録</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<a class="link text-info" href="EditorUpdateServlet">ID・パスワード変更</a>
		</div>
	</div>
</body>
</html>
