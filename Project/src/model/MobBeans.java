package model;

import java.io.Serializable;

public class MobBeans implements Serializable {

	private int id;
	private String name;
	private String map;
	private String lv;
	private String exp;
	private String element;
	private String category;
	private String remarks;

	public int getId() {
		return id;
	}

	public MobBeans (int id, String name) {
		this.id = id;
		this.name = name;
	}

	public MobBeans (String lv, String exp) {
		this.lv = lv;
		this.exp = exp;
	}

	public MobBeans (int id, String name, String remarks) {
		this.id = id;
		this.name = name;
		this.remarks = remarks;
	}

	public MobBeans (int id, String name, String map, String category) {
		this.id = id;
		this.name = name;
		this.map = map;
		this.category = category;
	}

	public MobBeans (int id, String name, String map, String lv, String exp, String element, String category, String remarks) {
		this.id = id;
		this.name = name;
		this.map = map;
		this.lv = lv;
		this.exp = exp;
		this.element = element;
		this.category = category;
		this.remarks = remarks;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMap() {
		return map;
	}
	public void setMap(String map) {
		this.map = map;
	}
	public String getLv() {
		return lv;
	}
	public void setLv(String lv) {
		this.lv = lv;
	}
	public String getExp() {
		return exp;
	}
	public void setExp(String exp) {
		this.exp = exp;
	}
	public String getElement() {
		return element;
	}
	public void setElement(String element) {
		this.element = element;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


}
