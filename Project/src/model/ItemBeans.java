package model;

import java.io.Serializable;

public class ItemBeans implements Serializable {

	private int id;
	private String name;
	private String category;
	private String def;
	private String atk;
	private String stability;
	private String detail;
	private String colorA;
	private String colorB;
	private String colorC;
	private String stack;
	private String cannot_trade;
	private String cannot_sale;
	private String hidden_property;
	private String remarks;

	public ItemBeans (int id, String name) {
		this.id = id;
		this.name = name;
	}

	public ItemBeans (int id, String name, String category, String def, String atk, String stability, String detail, String colorA, String colorB, String colorC, String stack, String cannot_trade, String cannot_sale, String hidden_property, String remarks) {
		this.id = id;
		this.name = name;
		this.category = category;
		this.def = def;
		this.atk = atk;
		this.stability = stability;
		this.detail = detail;
		this.colorA = colorA;
		this.colorB = colorB;
		this.colorC = colorC;
		this.stack = stack;
		this.cannot_trade = cannot_trade;
		this.cannot_sale = cannot_sale;
		this.hidden_property = hidden_property;
		this.remarks = remarks;
	}

	public ItemBeans (int id, String name, String category,String remarks) {
		this.id = id;
		this.name = name;
		this.category = category;
		this.remarks = remarks;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDef() {
		return def;
	}
	public void setDef(String def) {
		this.def = def;
	}
	public String getStability() {
		return stability;
	}
	public void setStability(String stability) {
		this.stability = stability;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getColorA() {
		return colorA;
	}
	public void setColorA(String colorA) {
		this.colorA = colorA;
	}
	public String getColorB() {
		return colorB;
	}
	public void setColorB(String colorB) {
		this.colorB = colorB;
	}
	public String getColorC() {
		return colorC;
	}
	public void setColorC(String colorC) {
		this.colorC = colorC;
	}
	public String getStack() {
		return stack;
	}
	public void setStack(String stack) {
		this.stack = stack;
	}
	public String getCannot_trade() {
		return cannot_trade;
	}
	public void setCannot_trade(String cannot_trade) {
		this.cannot_trade = cannot_trade;
	}
	public String getCannot_sale() {
		return cannot_sale;
	}
	public void setCannot_sale(String cannot_sale) {
		this.cannot_sale = cannot_sale;
	}
	public String getHidden_property() {
		return hidden_property;
	}
	public void setHidden_property(String hidden_property) {
		this.hidden_property = hidden_property;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getAtk() {
		return atk;
	}

	public void setAtk(String atk) {
		this.atk = atk;
	}



}
