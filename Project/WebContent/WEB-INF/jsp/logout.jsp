<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>【Logout】</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<br>
		<div class="row">
			<div class="col-auto mx-auto">
				<h2>
					<strong>Logout</strong>
				</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-auto mx-auto">
				ログアウト処理が完了しました。
			</div>
		</div>

	</div>
	<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<a class="link text-info" href="IndexServlet">TOPへ戻る</a>
		</div>
	</div>
</body>
</html>