package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ItemMobDao {

	//モブドロップデータの新規登録
	public int mobDropInsert (int mob_id, int item_id) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;
			String sql2;

			sql = "SELECT item_and_mob.id"
					+ " FROM item_and_mob"
					+ " WHERE mob_id = ? && item_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, mob_id);
			pStmt.setInt(2, item_id);

			ResultSet rs = pStmt.executeQuery();
			if(rs.next() != true) {

				sql2 = "INSERT INTO item_and_mob (mob_id, item_id)"
						+ " VALUES (?, ?)";
				PreparedStatement pStmt2 = conn.prepareStatement(sql2);
				pStmt2.setInt(1, mob_id);
				pStmt2.setInt(2, item_id);

				pStmt2.executeUpdate();

				return 0;
			} else {
				return 1;

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 2;

				}
			}

		}
	}

	//モブドロップデータの削除
	public int mobDropDelete (int mob_id, int item_id) {

		Connection conn = null;

		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//実行するSQL文
			String sql;
			String sql2;

			sql = "SELECT item_and_mob.id"
					+ " FROM item_and_mob"
					+ " WHERE mob_id = ? && item_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, mob_id);
			pStmt.setInt(2, item_id);

			ResultSet rs = pStmt.executeQuery();
			if(rs.next() == true) {

				sql2 = "DELETE FROM item_and_mob"
						+ " WHERE mob_id = ? && item_id = ?"
						+ " LIMIT 1";
				PreparedStatement pStmt2 = conn.prepareStatement(sql2);
				pStmt2.setInt(1, mob_id);
				pStmt2.setInt(2, item_id);

				pStmt2.executeUpdate();

				return 0;
			} else {
				return 1;

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		} finally {

			if(conn != null) {
				try {
					//データベース切断
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 2;

				}
			}

		}
	}

}
