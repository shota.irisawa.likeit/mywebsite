<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>【Login】</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<br>
		<div class="row">
			<div class="col-auto mx-auto">
				<h2>
					<strong>Login</strong>
				</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-auto mx-auto text-danger">${msg}</div>
		</div>

		<br>

		<form class="form-signin" action="LoginServlet" method="post" autocomplete="off">
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-2">ログインID</div>
				<div class="col-sm-6">
					<input type="text" name="loginId" style="width: 200px;"
						class="form-control">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-2">パスワード</div>
				<div class="col-sm-6">
					<input type="password" name="password" style="width: 200px;"
						class="form-control">
				</div>
			</div>
			<br> <br>
			<div class="row">
				<div class="col-auto mx-auto">
					<button class="btn btn-secondary btn-signin" style="width: 200px;"
						type="submit">ログイン</button>
				</div>
			</div>
		</form>
	</div>
	<br>
	<div class="row">
		<div class="col-auto mx-auto">
			<a class="link text-info" href="IndexServlet">TOPへ戻る</a>
		</div>
	</div>
</body>
</html>